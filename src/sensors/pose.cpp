#include <sensors/pose.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

  CPose::CPose(void) : CSensor_base(6)
  {
  }
   
  CPose::~CPose(void)
  {
  }

  void CPose::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
  {
    //Measurements noise.
    Eigen::VectorXf n = Eigen::VectorXf::Zero(6);
    if (std(0)!=0.0)
      n(0) = atools::get_rand() * std(0); 
    if (std(1)!=0.0)
      n(1) = atools::get_rand() * std(1);
    if (std(2)!=0.0)
      n(2) = atools::get_rand() * std(2);
    if (std(3)!=0.0)
      n(3) = atools::get_rand() * std(3);
    if (std(4)!=0.0)
      n(4) = atools::get_rand() * std(4);
    if (std(5)!=0.0)
      n(5) = atools::get_rand() * std(5);

    this->mutex_.lock();

    // To rotation rates
    Eigen::Quaternionf quat(x_state(6),x_state(7),x_state(8),x_state(9));
    Eigen::Vector3f theta; 
    atools::q2e(quat,theta);

    this->h_(0) = x_state(0) + n(0);
    this->h_(1) = x_state(1) + n(1);
    this->h_(2) = x_state(2) + n(2);
    this->h_(3) = theta(0) + n(3);
    this->h_(4) = theta(1) + n(4);
    this->h_(5) = theta(2) + n(5);

    this->mutex_.unlock();

    calc_e_obs_jac(x_state);
  }

  void CPose::calc_e_obs_jac(const Eigen::VectorXf& x_state)
  {
    this->mutex_.lock();
    this->H_dx_.block(0,0,3,3) = Eigen::MatrixXf::Identity(3,3);
    this->H_dx_.block(3,6,3,3) = Eigen::MatrixXf::Identity(3,3);
    this->mutex_.unlock();
  }

} //End of Sensor namespace