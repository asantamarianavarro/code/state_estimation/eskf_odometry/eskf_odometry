#ifndef _PX4_FLOW_H
#define _PX4_FLOW_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{

  /*! PX4 Sonar bound states class*/ 
  class PX4RangeBounds
  {
    public:
      /*! Sensor state w.r.t. range bounds*/ 
      enum State{ 
        BELOW, 
        INSIDE,
        ABOVE};
  };
  
  /*! PX4 Optical Flow parameters class*/ 
  class px4_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      bool using_laser;                  /*!< Using Sonar or pressure height the observation model and jacobian are different than laser pointer.*/
      px4_params() : CSensor_params(3)
      {
        this->using_laser = false; // using laser by default.
      };
      ~px4_params(){};
  };

  /*! PX4 Optical Flow data class*/ 
  class px4_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      px4_data() : CSensor_data(3)
      {
        this->uvflow = Eigen::Vector2f::Zero();
      };
      ~px4_data(){};
      Eigen::Vector2f uvflow;  /*!< Flow raw data: Flow_u and Flow_v (pixels/frame).*/
  };

  /*! Main PX4 Optical Flow Sensor class*/ 
	class CPx4_flow : public CSensor_base <px4_params,px4_data>
	{
	  private:

      float min_height_;                  /*!< Minimum height where ground is detected.*/ 
      float max_height_;                  /*!< Maximum height where ground is detected.*/ 
      int in_range_count_;                /*!< PX4 in range counter.*/
      int min_height_inliers_;            /*!< PX4 in range counter.*/
      PX4RangeBounds::State range_state_; /*!< Current range state.*/
	  	float last_gnd_dist;                /*!< To detect ground distance outliers.*/
      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.  
      *   - ang_vel: Current angular velocity (reading from Gyros). 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      */
      void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std = Eigen::VectorXf::Zero(3));

	  	/**
	    * \brief Compute Error-state PX4 Observation Jacobian
	    *
	    * Fills the PX4 Observation Jacobian matrix r.t. error state vector.
	    *
	    * Input:
		  * 	- x_state: Nominal state vector.
	    *	  - frame: Frame w.r.t. the Jacobian matrix is computed. 'l': local. 'g':global.	
	    */	
		  void calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame);

		  /**
	    * \brief Outliers Removal.
	    *
	    * Outliers Removal.
	    */
  		void remove_outliers(const Eigen::VectorXf& x_state, const Eigen::VectorXf& ang_vel);

      /**
      * \brief Check sensor bounds.
      *
      * Chech if sensor reading is in sensor bounds.
      */
      void check_sensor_bounds(const float& gnd_dist);

	  public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	  	/**
	    * \brief Constructor
	    *
	    * Optical Flow Class constructor.
	    *
	    */
	    CPx4_flow(void);

	  	/**
	    * \brief Destructor
	    *
	    * Optical Flow Class destructor.
	    *
	    */
	    ~CPx4_flow(void);

      /**
      * \brief Push data
      *
      * Push data into the buffer (back)
      */
      void push_data(const float& t, const Eigen::VectorXf& val);

	  	/**
	    * \brief Simulate PX4 readings
	    *
	    * Simulate and set PX4 readings
	    *
	    */
		  void simulate_readings(const Eigen::VectorXf& state, const float& t_now, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& px4_std_sim);

  };

} //End of Sensor namespace

#endif
