#ifndef _FLOW2D_H
#define _FLOW2D_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{
 
  /*! 2D Optical Flow parameters class*/ 
  class flow2d_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      flow2d_params(void) : CSensor_params(2) 
      {
        this->focal_length = 73; // IRI experiments
      };
      ~flow2d_params(void){};
      float focal_length;                /*!<  Focal length (pixels).*/
  };

  /*! 2D Optical Flow data class*/ 
  class flow2d_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      flow2d_data(void) : CSensor_data(2){};
      ~flow2d_data(void){};
  };

  /*! Main 2D Optical Flow (uv) Sensor class*/ 
  class CFlow2d : public CSensor_base<flow2d_params,flow2d_data>
  {
    private:

      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.  
      *   - ang_vel: Current angular velocity (reading from Gyros). 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      */
      void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std = Eigen::VectorXf::Zero(2));

    	/**
    	* \brief Compute Error-state Observation Jacobian
    	*
    	* Fills the Observation Jacobian matrix r.t. error state vector.
    	*
    	* Inputs:
    	*  - x_state: Nominal state vector.
    	*  - frame: Frame w.r.t. the Jacobian matrix is computed. 'l': local. 'g':global.	
    	*/	
    	void calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame);

    public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    	/**
      * \brief Constructor
      *
      * Optical Flow Class constructor.
      *
      */
      CFlow2d(void);

     	/**
      * \brief Destructor
      *
      * Optical Flow Class destructor.
      *
      */
      ~CFlow2d(void);
  };

} //End of Sensor namespace

#endif
