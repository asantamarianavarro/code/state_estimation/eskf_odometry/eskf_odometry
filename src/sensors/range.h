#ifndef _RANGE_H
#define _RANGE_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{

  /*! Range bound states class*/ 
  class RangeBounds
  {
    public:
      /*! Sensor state w.r.t. range bounds*/ 
      enum State{ 
        BELOW, 
        INSIDE,
        ABOVE};
  };

  /*! Range parameters class*/ 
  class range_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      float range_min;         /*!< Minimum height where ground is detected.*/
      float range_max;         /*!< Maximum height where ground is detected.*/
      bool using_laser;       /*!< Using Sonar or pressure height the observation model and jacobian are different than laser pointer.*/
      range_params() : CSensor_params(1) 
      {
        this->using_laser = false; // using laser by default.
        this->range_min = 0.31; // Teranger one bounds.
        this->range_max = 4.0; // Teraranger one bounds.       
      };
      ~range_params(){};
  };

  /*! Range data class*/ 
  class range_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      range_data() : CSensor_data(1){};
      ~range_data(){};
  };

  /*! Main Range Sensor class*/ 
  class CRange : public CSensor_base<range_params,range_data>
  {
    private:

      int in_range_count_;             /*!< Counter of measurements inside bounds.*/
      int range_min_inliers_;          /*!< Minimum inliers inside bounds.*/
      RangeBounds::State range_state_; /*!< Current range state.*/
      float last_gnd_dist;             /*!< To detect ground distance outliers.*/

      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.  
      *   - ang_vel: Current angular velocity (reading from Gyros). 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      */
      void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std = Eigen::VectorXf::Zero(1));

      /**
      * \brief Compute Error-state Observation Jacobian
      *
      * Fills the Observation Jacobian matrix r.t. error state vector.
      *
      * Input:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Jacobian matrix is computed. 'l': local. 'g':global.  
      */  
      void calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame);

      /**
      * \brief Outliers Removal.
      *
      * Outliers Removal.
      */
      void remove_outliers(const Eigen::VectorXf& x_state);

      /**
      * \brief Check sensor bounds.
      *
      * Chech if sensor reading is in sensor bounds.
      */
      void check_sensor_bounds(const float& gnd_dist);

    public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      /**
      * \brief Constructor
      *
      * Range Class constructor.
      *
      */
      CRange(void);

      /**
      * \brief Destructor
      *
      * Range Class destructor.
      *
      */
      ~CRange(void);
  };

} //End of Sensor namespace

#endif