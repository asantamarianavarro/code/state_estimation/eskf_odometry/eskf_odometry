#ifndef _QUADROTOR_H
#define _QUADROTOR_H

// Std stuff
#include <mutex>

namespace Robot{

  /*! Quadrotor phases*/ 
	typedef enum{
	  RUNNING,
	  STOPPED
	} quadrotor_phases;

  /*! Main Quadrotor class*/ 
	class CQuadrotor
	{

	  private:

	  	std::mutex mutex_; /*!< Mutex to lock/unlock the object.*/

	  public:

	  	quadrotor_phases phase; /*!< Quadrotor state.*/

	  	/**
	    * \brief Constructor
	    *
	    * Quadrotor Class constructor.
	    *
	    */
	    CQuadrotor();

	  	/**
	    * \brief Destructor
	    *
	    * Quadrotor Class destructor.
	    *
	    */
	    ~CQuadrotor();

	  	/**
	    * \brief Lock
	    *
	    * Lock Object.
	    *
	    */
	    void lock();

	  	/**
	    * \brief Unlock
	    *
	    * Unlock Object.
	    *
	    */
	    void unlock();	 

	  	/**
	    * \brief Set phase
	    *
	    * Set Quadrotor phase
	    *
	    */
		  void set_phase(const quadrotor_phases& p);

	  	/**
	    * \brief Get phase	    
	    *
	    * Get Quadrotor phase
	    *
	    */
		  void get_phase(quadrotor_phases& s);

	    /**
	    * \brief Print Robot phase
	    *
	    * This function is used to print the robot flying phase:
	    * Landed, Taking-Off, Flying or Landing.
	    *
	    */
	    void print_phase(void);		
	};
	
} //End of Sensors namespace

#endif
