#include <sensors/position.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

  CPosition::CPosition(void) : CSensor_base(3)
  {
  }
   
  CPosition::~CPosition(void)
  {
  }

  void CPosition::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
  {
    //Measurements noise.
    Eigen::Vector3f n = Eigen::Vector3f::Zero();
    if (std(0)!=0.0)
      n(0) = atools::get_rand() * std(0); 
    if (std(1)!=0.0)
      n(1) = atools::get_rand() * std(1);
    if (std(2)!=0.0)
      n(2) = atools::get_rand() * std(2);

    this->mutex_.lock();

    this->h_(0) = x_state(0) + n(0);
    this->h_(1) = x_state(1) + n(1);
    this->h_(2) = x_state(2) + n(2);

    this->mutex_.unlock();

    calc_e_obs_jac(x_state);
  }

  void CPosition::calc_e_obs_jac(const Eigen::VectorXf& x_state)
  {
    this->mutex_.lock();
    this->H_dx_.block(0,0,3,3) = Eigen::Matrix3f::Identity();
    this->mutex_.unlock();
  }

} //End of Sensor namespace