#ifndef _POSE_SENSOR_H
#define _POSE_SENSOR_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{

  /*! Pose parameters class*/ 
  class pose_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      pose_params(void) : CSensor_params(6){};
      ~pose_params(void){};
  };

  /*! Pose data class*/ 
  class pose_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      pose_data(void) : CSensor_data(6){};
      ~pose_data(void){};
  };

  /*! Main Pose Sensor class*/ 
  class CPose : public CSensor_base<pose_params,pose_data>
  {
    private:

      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.  
      *   - ang_vel: Current angular velocity (reading from Gyros). 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      */
      void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std = Eigen::VectorXf::Zero(6));

      /**
      * \brief Compute Error-state Observation Jacobian
      *
      * Fills the Observation Jacobian matrix r.t. error state vector.
      *
      * Input:
      *   - x_state: Nominal state vector.
      */  
      void calc_e_obs_jac(const Eigen::VectorXf& x_state);

    public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      /**
      * \brief Constructor
      *
      * Optical Flow Class constructor.
      *
      */
      CPose(void);

      /**
      * \brief Destructor
      *
      * Optical Flow Class destructor.
      *
      */
      ~CPose(void);
  };

} //End of Sensor namespace

#endif