#include <sensors/range.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

  CRange::CRange(void) : CSensor_base(1)
  {
    this->last_gnd_dist = 0.0; // Initially considered on the ground.
    this->in_range_count_ = 0;  // Counters to detect if it is in range or not.
    this->range_min_inliers_ = 20; // Min good values to consider in range.
    this->range_state_ = RangeBounds::State::BELOW; // Considering robot on the ground.
  }
   
  CRange::~CRange(void)
  {
  }

  void CRange::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
  {
    Eigen::Matrix3f R;
    Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
    atools::q2R(q,R);

    // Relative orientation of the Range sensor (extrinsics calibration)
    Eigen::Matrix3f Rr;
    Eigen::Quaternionf qr(x_state(29),x_state(30),x_state(31),x_state(32));
    atools::q2R(qr,Rr);

    //Measurements noise.
    Eigen::VectorXf n = Eigen::VectorXf::Zero(1);
    if (std(0)!= 0.0)
      n(0) = atools::get_rand() * std(0);

    Eigen::MatrixXf Sz = Eigen::MatrixXf::Zero(1,3);
    Sz(2) = 1.0;

    Eigen::Vector3f pr; // realtive sensor position wrt IMU
    pr << x_state(24),x_state(25),x_state(26);

    this->mutex_.lock();
    if (!this->params_.using_laser)
      this->h_ = Sz*(Rr.transpose()*x_state.segment(0,3) + pr) + n;
    else
      this->h_ = Sz*(Rr.transpose()*R.transpose()*x_state.segment(0,3) + pr) + n;

    this->mutex_.unlock();

    // Compute Observation Jacobian
    calc_e_obs_jac(x_state, frame);

    if (!is_queue_empty())
    {
      // Remove outliers
      remove_outliers(x_state);
  
      // Set Corresponding STD depending on sensor bounds
      check_sensor_bounds(this->data_queue_.front().val(0));
    }
  }

  void CRange::calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame)
  {
    Eigen::Matrix3f R;
    Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
    atools::q2R(q,R);

    // Relative orientation of the Range sensor (extrinsics calibration)
    Eigen::Matrix3f Rr;
    Eigen::Quaternionf qr(x_state(29),x_state(30),x_state(31),x_state(32));
    atools::q2R(qr,Rr);

    Eigen::Matrix3f tmpR;
    Eigen::Matrix3f tmpRr;

    if(frame=='l') // Local frame
    {
      if (!this->params_.using_laser)
        atools::v2skew(Rr.transpose()*x_state.segment(3,3),tmpRr); // 1rst row: z (due to sensor orientation)
      else
      {  
        atools::v2skew(R.transpose()*x_state.segment(3,3),tmpR); // 1rst row: z (due to orientation)
        tmpR = Rr.transpose() * tmpR;
        atools::v2skew(Rr.transpose()*R.transpose()*x_state.segment(3,3),tmpRr); // 1rst row: z (due to sensor orientation)
      }
    }
    else  // Global frame
    {
      Eigen::Matrix3f tmpsk;
      atools::v2skew(x_state.segment(3,3),tmpsk);

      if (!this->params_.using_laser)
        tmpRr = Rr.transpose() * tmpsk; //  1rst row: z (due to sensor orientation)
      else
      {
        tmpR = Rr.transpose() * R.transpose() * tmpsk; //  1rst row: z (due to orientation)
        atools::v2skew(R.transpose()*x_state.segment(3,3),tmpsk);
        tmpRr = Rr.transpose() * tmpsk; //  1rst row: z (due to sensor orientation)
      }
    } 

    Eigen::MatrixXf Sz = Eigen::MatrixXf::Zero(1,3);
    Sz(2) = 1.0;

    this->mutex_.lock();

    if (!this->params_.using_laser)
      this->H_dx_.block(0,0,1,3) = Sz * Rr.transpose(); // position
    else
    {
      this->H_dx_.block(0,0,1,3) = Sz * Rr.transpose() * R.transpose(); // position
      this->H_dx_.block(0,6,1,3) = Sz * tmpR; // orientation
    }

    this->H_dx_.block(0,24,1,3) = Sz; // sensor position
    this->H_dx_.block(0,27,1,3) = Sz * tmpRr; // sensor orientation
    this->mutex_.unlock();
  }

  void CRange::remove_outliers(const Eigen::VectorXf& x_state)
  {
    this->mutex_.lock();
    // Detect height outliers
    if(abs(this->data_queue_.front().val(0)-this->last_gnd_dist)>0.35)
    {
      atools::print("Warning: Range outlier detected: ",yellow);
      atools::print("Read value: ",blue);
      atools::print(this->data_queue_.front().val(0),white);
      this->data_queue_.front().val(0) = x_state(2);
      atools::print(" Corrected with values: ",blue);
      atools::print(this->data_queue_.front().val(0),white);
      atools::print("\n",blue);
    } 
    this->last_gnd_dist = this->data_queue_.front().val(0);
    this->mutex_.unlock();
  } 

  void CRange::check_sensor_bounds(const float& gnd_dist)
  {
    if ((gnd_dist > this->params_.range_min) && (gnd_dist < this->params_.range_max))
    {
      if(this->in_range_count_ < this->range_min_inliers_)
        this->in_range_count_ = this->in_range_count_ + 1;
    }
    if((gnd_dist < this->params_.range_min) || (gnd_dist > this->params_.range_max))
    {
      if(this->in_range_count_ > 0)
        this->in_range_count_ = this->in_range_count_ - 1;
    }

    switch (this->range_state_) 
    {
      case RangeBounds::State::BELOW:
        if (this->in_range_count_ == this->range_min_inliers_)
        {
          this->params_.std = this->params_.std_insidebounds;
          this->range_state_ = RangeBounds::State::INSIDE;
        }
      break;
      case RangeBounds::State::INSIDE:
        if (gnd_dist>this->params_.range_max)
        {
          this->params_.std = this->params_.std_outsidebounds;
          this->range_state_ = RangeBounds::State::ABOVE;
        }
        if (gnd_dist<this->params_.range_min)
        {
          this->params_.std = this->params_.std_outsidebounds;
          this->range_state_ = RangeBounds::State::BELOW;
        }
      break;
      case RangeBounds::State::ABOVE:
        if (this->in_range_count_ == this->range_min_inliers_)
        {
          this->params_.std = this->params_.std_insidebounds;
          this->range_state_ = RangeBounds::State::INSIDE;
        }
      break;
    }
  }
} //End of Sensor namespace
