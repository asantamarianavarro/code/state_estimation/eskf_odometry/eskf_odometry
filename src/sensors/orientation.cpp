#include <sensors/orientation.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

  COrientation::COrientation(void) : CSensor_base(3)
  {
  }
   
  COrientation::~COrientation(void)
  {
  }

  void COrientation::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
  {
    //Measurements noise.
    Eigen::Vector3f n = Eigen::Vector3f::Zero();
    if (std(0)!=0.0)
      n(0) = atools::get_rand() * std(0); 
    if (std(1)!=0.0)
      n(1) = atools::get_rand() * std(1);
    if (std(2)!=0.0)
      n(2) = atools::get_rand() * std(2);

    this->mutex_.lock();

    // To rotation rates
    Eigen::Quaternionf quat(x_state(6),x_state(7),x_state(8),x_state(9));
    Eigen::Vector3f theta; 
    atools::q2e(quat,theta);

    this->h_(0) = theta(0) + n(0);
    this->h_(1) = theta(1) + n(1);
    this->h_(2) = theta(2) + n(2);

    this->mutex_.unlock();

    calc_e_obs_jac(x_state);
  }

  void COrientation::calc_e_obs_jac(const Eigen::VectorXf& x_state)
  {
    this->mutex_.lock();
    this->H_dx_.block(0,6,3,3) = Eigen::Matrix3f::Identity();
    this->mutex_.unlock();
  }
} //End of Sensor namespace