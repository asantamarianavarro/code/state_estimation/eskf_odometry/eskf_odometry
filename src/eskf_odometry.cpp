#include "eskf_odometry.h"

// std stuff
#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h> 
#include <stdlib.h>
#include <time.h> 
#include <map>

// atools stuff
#include <debug_fc.h>
#include <rot_fc.h>

CEskf::CEskf(void)
{
    this->mutex_.lock();

    // Initialize frame
    this->params_.frame = 'g';

    // Barcelona gravity vector
    this->g_ << 0.0,0.0,-9.803057;

    // Initialize nominal state vector
    this->x_ = Eigen::VectorXf::Zero(33);
    this->x_(6) = 1.0; // q_w
    this->x_(22) = 1.0; // qo_w
    this->x_(29) = 1.0; // qr_w
    this->x_.segment(16,3) = this->g_;

    // Initialize error state vector
    this->dx_ = Eigen::VectorXf::Zero(30);

    // Initialize Estimation convariance
    this->P_ = Eigen::MatrixXf::Zero(30,30);

    // First propagation flag
    this->first_prop_done_ = false;

    // Set Robot stopped
    this->quadrotor_.set_phase(Robot::STOPPED);
    this->quadrotor_.print_phase();

    // Sensor pointers
    this->imu_         = std::make_shared<Sensor::CImu>();
    this->pose_        = std::make_shared<Sensor::CPose>();
    this->pose2_       = std::make_shared<Sensor::CPose>();
    this->position_    = std::make_shared<Sensor::CPosition>();
    this->orientation_ = std::make_shared<Sensor::COrientation>();
    this->linvel_      = std::make_shared<Sensor::CLinvel>();
    this->range_       = std::make_shared<Sensor::CRange>();
    this->px4_         = std::make_shared<Sensor::CPx4_flow>();
    this->flow2d_      = std::make_shared<Sensor::CFlow2d>();

    this->mutex_.unlock();
}

CEskf::~CEskf(void)
{
}

//******************************************************************************
//************* FILTER *********************************************************
//******************************************************************************
void CEskf::sim_sequence(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& imu_std_sim, const Eigen::VectorXf& position_std_sim, const Eigen::VectorXf& orientation_std_sim, const Eigen::VectorXf& linvel_std_sim, const Eigen::VectorXf& range_std_sim, const Eigen::VectorXf& px4_std_sim, const Eigen::VectorXf& flow2d_std_sim, const float& tstamp, const char& frame)
{
    // Time
    this->set_time(tstamp);

    this->mutex_.lock();

    // Simulate IMU readings
    this->imu_->simulate_readings(a,w,g,imu_std_sim,this->stamp_.dt,frame,this->x_,this->stamp_.t_now);

    //Robot considered RUNNING during the simulation and all sensors in range.
    if (!this->imu_->is_queue_empty())
    {
        Eigen::Vector3f wimu = this->imu_->get_earliest_data().val.segment(3,3);
        Eigen::VectorXf pose_std_sim(6);
        pose_std_sim << position_std_sim, orientation_std_sim;
        this->pose_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, pose_std_sim);
        this->pose2_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, pose_std_sim);
        this->position_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, position_std_sim);
        this->orientation_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, orientation_std_sim);
        this->linvel_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, linvel_std_sim);
        this->range_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, range_std_sim);
        this->px4_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, px4_std_sim);
        this->flow2d_->simulate_readings(this->x_, this->stamp_.t_now, frame, wimu, flow2d_std_sim);
    }

    this->mutex_.unlock();
}

void CEskf::set_init_params(const params& f_params, const Eigen::VectorXf& x0, const Eigen::VectorXf& dx0, const Sensor::imu_params& imu, const Sensor::pose_params& pose, const Sensor::pose_params& pose2, const Sensor::position_params& position, const Sensor::orientation_params& orientation, const Sensor::linvel_params& linvel, const Sensor::range_params& range, const Sensor::px4_params& px4, const Sensor::flow2d_params& flow2d)
{
    set_params(f_params);
    this->mutex_.lock();
    this->x_ = x0;
    this->x_atstop_ = this->x_;
    this->dx_ = dx0;
    this->mutex_.unlock();
    this->imu_->set_params(imu);
    this->pose_->set_params(pose);
    this->pose2_->set_params(pose2);
    this->position_->set_params(position);
    this->orientation_->set_params(orientation);
    this->linvel_->set_params(linvel);
    this->range_->set_params(range);
    this->px4_->set_params(px4);
    this->flow2d_->set_params(flow2d);
    this->mutex_.unlock();
}

void CEskf::set_params(const params& p)
{
    this->mutex_.lock();
    this->params_ = p;
    this->mutex_.unlock();

    set_Pcov_mat();
}

void CEskf::set_Pcov_mat(void)
{
    this->mutex_.lock();
    Eigen::VectorXf f_std_ini = Eigen::VectorXf::Zero(30);
    f_std_ini.block(0,0,3,1) = this->params_.dp0_std.array()*this->params_.dp0_std.array();
    f_std_ini.block(3,0,3,1) = this->params_.dv0_std.array()*this->params_.dv0_std.array();
    f_std_ini.block(6,0,3,1) = this->params_.dtheta0_std.array()*this->params_.dtheta0_std.array();
    f_std_ini.block(9,0,3,1) = this->params_.dab0_std.array()*this->params_.dab0_std.array();
    f_std_ini.block(12,0,3,1) = this->params_.dwb0_std.array()*this->params_.dwb0_std.array();
    f_std_ini.block(15,0,3,1) = this->params_.dg0_std.array()*this->params_.dg0_std.array();
    f_std_ini.block(18,0,3,1) = this->params_.dpo0_std.array()*this->params_.dpo0_std.array();
    f_std_ini.block(21,0,3,1) = this->params_.dthetao0_std.array()*this->params_.dthetao0_std.array();
    f_std_ini.block(24,0,3,1) = this->params_.dpr0_std.array()*this->params_.dpr0_std.array();
    f_std_ini.block(27,0,3,1) = this->params_.dthetar0_std.array()*this->params_.dthetar0_std.array();
    this->P_ = f_std_ini.asDiagonal();
    this->mutex_.unlock();
}

void CEskf::set_time(const float& tstamp)
{
    this->mutex_.lock();
    this->stamp_.t_old = this->stamp_.t_now;
    this->stamp_.t_now = tstamp;
    this->stamp_.dt = this->stamp_.t_now - this->stamp_.t_old;
    this->mutex_.unlock();
}

float CEskf::get_proc_time(void)
{
    float proc_time;
    this->mutex_.lock();
    proc_time = this->stamp_.proc_time;
    this->mutex_.unlock();
    return proc_time;
}

void CEskf::set_obs_phase(const bool& flying, const float& gnd_dist)
{
    this->mutex_.lock();
    Robot::quadrotor_phases phase;
    this->quadrotor_.get_phase(phase);
    switch (phase) {
        case Robot::STOPPED:
            if (flying)
            {
                this->quadrotor_.set_phase(Robot::RUNNING);
                this->quadrotor_.print_phase();
            }
            break;
        case Robot::RUNNING:
            if (!flying)
            {
                this->quadrotor_.set_phase(Robot::STOPPED);
                this->quadrotor_.print_phase();
                this->x_atstop_ = this->x_;
            }
            break;
    }
    this->mutex_.unlock();
}

Eigen::VectorXf CEskf::get_x_state(void)
{
    Eigen::VectorXf x;
    this->mutex_.lock();
    x = this->x_;
    this->mutex_.unlock();
    return x;
}

Eigen::VectorXf CEskf::get_dx_state(void)
{
    Eigen::VectorXf dx;
    this->mutex_.lock();
    dx = this->dx_;
    this->mutex_.unlock();
    return dx;
}

bool CEskf::update(Eigen::VectorXf& state, Eigen::Vector3f& ang_vel, const bool& flying, const float& gnd_dist)
{
    set_obs_phase(flying,gnd_dist);

    Sensor_Name sensor;
    float tstamp;
    bool first_imu = true;
    bool processed = false;
    bool correction_done = false;

    if (which_next(sensor, tstamp))
    {
        string name;
        switch(sensor){
            case IMU:
                name = "IMU";
                this->w_ = this->imu_->get_earliest_data().val.segment(3,3);
                this->stamp_.proc_time = this->imu_->get_earliest_data().tstamp;
                if (this->imu_->is_first_reading())
                    first_imu = false;
                else if(!this->imu_->is_queue_empty())
                {
                    this->imu_->prop_state(this->imu_->get_earliest_data(), this->x_, this->params_.frame, this->P_);
                    this->first_prop_done_ = true;
                }
                break;
            case POSE:
                name = "POSE";
                correction_done = sensor_correction(this->pose_);
                break;
            case POSE2:
                name = "POSE2";
                correction_done = sensor_correction(this->pose2_);
                break;
            case POSITION:
                name = "POSITION";
                correction_done = sensor_correction(this->position_);
                break;
            case ORIENTATION:
                name = "ORIENTATION";
                correction_done = sensor_correction(this->orientation_);
                break;
            case LINVEL:
                name = "LINVEL";
                correction_done = sensor_correction(this->linvel_);
                break;
            case RANGE:
                name = "RANGE";
                correction_done = sensor_correction(this->range_);
                break;
            case PX4:
                name = "PX4";
                correction_done = sensor_correction(this->px4_);
                break;
            case FLOW2D:
                name = "FLOW2D";
                correction_done = sensor_correction(this->flow2d_);
                break;
        }
        if (sensor!=IMU && correction_done)
        {
            // Filter Update
            eskf_update();

            // Reset
            eskf_reset();
        }

        // std::cout << name << " @: " << this->stamp_.proc_time << std::endl;

        state = get_x_state();

        Eigen::Vector3f ang_vel_bias;
        ang_vel_bias << state(13),state(14),state(15);
        ang_vel = this->w_ - ang_vel_bias;

        processed = true;
    }
    return processed;
}

bool CEskf::which_next(Sensor_Name& name, float& tstamp)
{
    this->mutex_.lock();
    map<float,Sensor_Name> sensor_list;
    if (!this->imu_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->imu_->get_earliest_data().tstamp,IMU));
    if (!this->pose_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->pose_->get_earliest_data().tstamp,POSE));
    if (!this->pose2_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->pose2_->get_earliest_data().tstamp,POSE2));
    if (!this->position_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->position_->get_earliest_data().tstamp,POSITION));
    if (!this->orientation_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->orientation_->get_earliest_data().tstamp,ORIENTATION));
    if (!this->linvel_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->linvel_->get_earliest_data().tstamp,LINVEL));
    if (!this->range_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->range_->get_earliest_data().tstamp,RANGE));
    if (!this->px4_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->px4_->get_earliest_data().tstamp,PX4));
    if (!this->flow2d_->is_queue_empty())
        sensor_list.insert(pair<float,Sensor_Name>(this->flow2d_->get_earliest_data().tstamp,FLOW2D));

    bool filled = false;
    if (!sensor_list.empty())
    {
        filled = true;
        tstamp = sensor_list.begin()->first;
        name = sensor_list.begin()->second;
    }
    this->mutex_.unlock();
    return filled;
}

void CEskf::eskf_update(void)
{
    this->mutex_.lock();

    this->x_.segment(0,6) = this->x_.segment(0,6) + this->dx_.segment(0,6); // p and v
    this->x_.segment(10,9) = this->x_.segment(10,9) + this->dx_.segment(9,9); // ab, wb and g
    this->x_.segment(19,3) = this->x_.segment(19,3) + this->dx_.segment(18,3); // po
    this->x_.segment(26,3) = this->x_.segment(26,3) + this->dx_.segment(24,3); // pr
    Eigen::Quaternionf dxq; //Relative error rotation
    atools::v2q(this->dx_.segment(6,3),dxq);
    Eigen::Quaternionf xq(this->x_(6),this->x_(7),this->x_(8),this->x_(9)); //Old global rotation
    Eigen::Quaternionf dxqo; //Relative error rotation OF sensor
    atools::v2q(this->dx_.segment(21,3),dxqo);
    Eigen::Quaternionf xqo(this->x_(22),this->x_(23),this->x_(24),this->x_(25)); //Old global rotation
    Eigen::Quaternionf dxqr; //Relative error rotation
    atools::v2q(this->dx_.segment(27,3),dxqr);
    Eigen::Quaternionf xqr(this->x_(29),this->x_(30),this->x_(31),this->x_(32)); //Old global rotation

    if (this->params_.frame == 'l') //Local frame
    {
        atools::qProd(xq,dxq,xq);    // Orientation concatenation
        atools::qProd(xqo,dxqo,xqo); // OF Sensor orientation concatenation
        atools::qProd(xqr,dxqr,xqr); // Rnge Sensor orientation concatenation
    }
    else //Global frame
    {
        atools::qProd(dxq,xq,xq);    // Orientation concatenation
        atools::qProd(dxqo,xqo,xqo); // OF Sensor orientation concatenation
        atools::qProd(dxqr,xqr,xqr); // Rnge Sensor orientation concatenation
    }

    this->x_.segment(6,4) << xq.normalized().w(),xq.normalized().x(),xq.normalized().y(),xq.normalized().z();
    this->x_.segment(22,4) << xqo.normalized().w(),xqo.normalized().x(),xqo.normalized().y(),xqo.normalized().z();
    this->x_.segment(29,4) << xqr.normalized().w(),xqr.normalized().x(),xqr.normalized().y(),xqr.normalized().z();

    this->mutex_.unlock();
}

void CEskf::eskf_reset(void)
{
    this->mutex_.lock();

    Eigen::Matrix3f R;
    Eigen::Matrix3f Ro;
    Eigen::Matrix3f Rr;

    if (this->params_.frame == 'l')
    {
        atools::v2R(-this->dx_.segment(6,3)/2.0,R);
        atools::v2R(-this->dx_.segment(21,3)/2.0,Ro);
        atools::v2R(-this->dx_.segment(27,3)/2.0,Rr);
    }
    else
    {
        atools::v2R(this->dx_.segment(6,3)/2.0,R);
        atools::v2R(this->dx_.segment(21,3)/2.0,Ro);
        atools::v2R(this->dx_.segment(27,3)/2.0,Rr);
    }

    //Reset Jacobian
    Eigen::MatrixXf G = Eigen::MatrixXf::Identity(30,30);
    G.block(6,6,3,3) = R;
    G.block(21,21,3,3) = Ro;
    G.block(27,27,3,3) = Rr;

    this->P_ = G * this->P_ * G.transpose();

    this->dx_ = Eigen::VectorXf::Zero(30);

    this->mutex_.unlock();
}

void CEskf::print_ini_params(void)
{
    atools::print("\n_________________________________________ESKF_odom, loaded parameters\n",yellow);

    // Filter parameters
    atools::print("  ______Filter params (STD and frame)",green);
    atools::print("\n  dp0_std: ",blue);
    atools::print(this->params_.dp0_std.transpose(),white);
    atools::print("\n  dv0_std: ",blue);
    atools::print(this->params_.dv0_std.transpose(),white);
    atools::print("\n  dtheta0_std: ",blue);
    atools::print(this->params_.dtheta0_std.transpose(),white);
    atools::print("\n  dab0_std: ",blue);
    atools::print(this->params_.dab0_std.transpose(),white);
    atools::print("\n  dwb0_std: ",blue);
    atools::print(this->params_.dwb0_std.transpose(),white);
    atools::print("\n  dg0_std: ",blue);
    atools::print(this->params_.dg0_std.transpose(),white);
    atools::print("\n  dpo0_std: ",blue);
    atools::print(this->params_.dpo0_std.transpose(),white);
    atools::print("\n  dthetao0_std: ",blue);
    atools::print(this->params_.dthetao0_std.transpose(),white);
    atools::print("\n  dpr0_std: ",blue);
    atools::print(this->params_.dpr0_std.transpose(),white);
    atools::print("\n  dthetar0_std: ",blue);
    atools::print(this->params_.dthetar0_std.transpose(),white);
    cout << endl;

    this->imu_->print_params(); // IMU STD values
    this->pose_->print_params(); // POSE STD values
    this->pose2_->print_params(); // POSE 2 STD values
    this->position_->print_params(); // POSITION STD values
    this->orientation_->print_params(); // ORIENTATION STD values
    this->linvel_->print_params(); // LINEAR VELOCITY STD values
    this->range_->print_params(); // RANGE STD values
    this->px4_->print_params(); // PX4 STD values
    this->flow2d_->print_params(); // FLOW2D STD values

    // Nominal-State vector: Warning, if this function is not called initially,
    // the printed values won't correspond to the initial vector.
    atools::print("  ______Initial Nominal-State Vector.",green);
    atools::print("\n  x_state: ",blue);
    atools::print(this->x_.transpose(),white);
    cout << endl;

    // Error-State vector: Warning, if this function is not called initially,
    // the printed values won't correspond to the initial vector.
    atools::print("  ______Initial Error-State Vector.",green);
    atools::print("\n  dx_state: ",blue);
    atools::print(this->dx_.transpose(),white);
    cout << endl;
}

// *****************************************************************************
// IMU *************************************************************************
// *****************************************************************************

Eigen::VectorXf CEskf::get_imu_data(void)
{ return get_sensor_data(this->imu_); }
Sensor::imu_params CEskf::get_imu_params(void)
{ return this->imu_->get_params(); }
void CEskf::set_imu_params(Sensor::imu_params& imu)
{ this->imu_->set_params(imu); }
void CEskf::set_imu_reading(const float& t, const Eigen::Vector3f& a, const Eigen::Vector3f& w)
{
    this->mutex_.lock();
    this->imu_->push_data(t,a,w);
    this->mutex_.unlock();

    // Using POSE sensor to help Filter converge the first steps and keep the robot static when it is not moving.
    Robot::quadrotor_phases phase;
    this->quadrotor_.get_phase(phase);
    if (phase == Robot::STOPPED && this->first_prop_done_)
    {
        Sensor::pose_params pose_params;
        pose_params.std << 0.001,0.001,0.001,0.001,0.001,0.001; // Low values is enough.
        this->pose_->set_params(pose_params);
        Eigen::Vector3f eul;
        Eigen::Quaternionf quat(this->x_atstop_(6),this->x_atstop_(7),this->x_atstop_(8),this->x_atstop_(9));
        atools::q2e(quat,eul);
        Eigen::VectorXf landed_val(6);
        landed_val << this->x_atstop_(0),this->x_atstop_(1),this->x_atstop_(2),eul;
        set_pose_reading(t+0.01,landed_val);
    }
}

// *****************************************************************************
// POSE ************************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_pose_data(void)
{ return get_sensor_data(this->pose_); }
Sensor::pose_params CEskf::get_pose_params(void)
{ return get_sensor_params<Sensor::pose_params>(this->pose_); }
void CEskf::set_pose_params(Sensor::pose_params& pose)
{ set_sensor_params(this->pose_,pose); }
void CEskf::set_pose_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->pose_,t,val); }

Eigen::VectorXf CEskf::get_pose2_data(void)
{ return get_sensor_data(this->pose2_); }
Sensor::pose_params CEskf::get_pose2_params(void)
{ return get_sensor_params<Sensor::pose_params>(this->pose2_); }
void CEskf::set_pose2_params(Sensor::pose_params& pose)
{ set_sensor_params(this->pose2_,pose); }
void CEskf::set_pose2_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->pose2_,t,val); }

// *****************************************************************************
// POSITION ********************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_position_data(void)
{ return get_sensor_data(this->position_); }
Sensor::position_params CEskf::get_position_params(void)
{ return get_sensor_params<Sensor::position_params>(this->position_); }
void CEskf::set_position_params(Sensor::position_params& position)
{ set_sensor_params(this->position_,position); }
void CEskf::set_position_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->position_,t,val); }

// *****************************************************************************
// ORIENTATION *****************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_orientation_data(void)
{ return get_sensor_data(this->orientation_); }
Sensor::orientation_params CEskf::get_orientation_params(void)
{ return get_sensor_params<Sensor::orientation_params>(this->orientation_); }
void CEskf::set_orientation_params(Sensor::orientation_params& orientation)
{ set_sensor_params(this->orientation_,orientation); }
void CEskf::set_orientation_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->orientation_,t,val); }

// *****************************************************************************
// LINEAR VELOCITY *************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_linvel_data(void)
{ return get_sensor_data(this->linvel_); }
Sensor::linvel_params CEskf::get_linvel_params(void)
{ return get_sensor_params<Sensor::linvel_params>(this->linvel_); }
void CEskf::set_linvel_params(Sensor::linvel_params& linvel)
{ set_sensor_params(this->linvel_,linvel); }
void CEskf::set_linvel_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->linvel_,t,val); }

// *****************************************************************************
// RANGE ***********************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_range_data(void)
{ return get_sensor_data(this->range_); }
Sensor::range_params CEskf::get_range_params(void)
{ return get_sensor_params<Sensor::range_params>(this->range_); }
void CEskf::set_range_params(Sensor::range_params& range)
{ set_sensor_params(this->range_,range); }
void CEskf::set_range_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->range_,t,val); }

// *****************************************************************************
// PX4 *************************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_px4_data(void)
{ return get_sensor_data(this->px4_); }
Sensor::px4_params CEskf::get_px4_params(void)
{ return get_sensor_params<Sensor::px4_params>(this->px4_); }
void CEskf::set_px4_params(Sensor::px4_params& px4)
{ set_sensor_params(this->px4_,px4); }
void CEskf::set_px4_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->px4_,t,val); }

// *****************************************************************************
// FLOW2D **********************************************************************
// *****************************************************************************
Eigen::VectorXf CEskf::get_flow2d_data(void)
{ return get_sensor_data(this->flow2d_); }
Sensor::flow2d_params CEskf::get_flow2d_params(void)
{ return get_sensor_params<Sensor::flow2d_params>(this->flow2d_); }
void CEskf::set_flow2d_params(Sensor::flow2d_params& flow2d)
{ set_sensor_params(this->flow2d_,flow2d); }
void CEskf::set_flow2d_reading(const float& t, const Eigen::VectorXf& val)
{ set_sensor_reading(this->flow2d_,t,val); }
