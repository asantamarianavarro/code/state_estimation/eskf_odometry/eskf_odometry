#ifndef _ESKF_ODOMETRY_H
#define _ESKF_ODOMETRY_H

// Std stuff
#include <mutex>
#include <map>
#include <memory>

// Boost stuff
#include <boost/variant.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensors
#include <sensors/imu.h>
#include <sensors/pose.h>
#include <sensors/position.h>
#include <sensors/orientation.h>
#include <sensors/linvel.h>
#include <sensors/range.h>
#include <sensors/px4_flow.h>
#include <sensors/flow2d.h>

// Robot
#include <robots/quadrotor.h>

typedef std::shared_ptr<Sensor::CSensor_superbase> base_ptr;

/*! Sensor names*/
enum Sensor_Name { 
    IMU, 
    POSE,
    POSE2,
    POSITION,
    ORIENTATION,
    LINVEL,
    RANGE,
    PX4,
    FLOW2D};

/*! Filter parameters class */ 
class params{
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    params()
    {
      // class variable initializations
      this->dp0_std = Eigen::Vector3f::Zero(); 
      this->dv0_std = Eigen::Vector3f::Zero();
      this->dtheta0_std = Eigen::Vector3f::Zero(); 
      this->dab0_std = Eigen::Vector3f::Zero();
      this->dwb0_std = Eigen::Vector3f::Zero(); 
      this->dg0_std = Eigen::Vector3f::Zero(); 
      this->dpo0_std = Eigen::Vector3f::Zero(); 
      this->dthetao0_std = Eigen::Vector3f::Zero(); 
      this->dpr0_std = Eigen::Vector3f::Zero(); 
      this->dthetar0_std = Eigen::Vector3f::Zero(); 
    };
    ~params(){};
    Eigen::Vector3f dp0_std;       /*!< Initial delta-p std dev (Position).*/
    Eigen::Vector3f dv0_std;       /*!< Initial delta-v std dev (Velocity).*/
    Eigen::Vector3f dtheta0_std;   /*!< Initial delta-theta std dev (Orientation).*/
    Eigen::Vector3f dab0_std;      /*!< Initial delta-ab std dev (Acc. biases).*/
    Eigen::Vector3f dwb0_std;      /*!< Initial delta-wb std dev (Gyro biases).*/
    Eigen::Vector3f dg0_std;       /*!< Initial delta-g std dev (Gravity).*/
    Eigen::Vector3f dpo0_std;     /*!< Initial delta-po std dev (OF sensor position).*/
    Eigen::Vector3f dthetao0_std; /*!< Initial delta-thetao std dev (OF sensor orientation).*/
    Eigen::Vector3f dpr0_std;      /*!< Initial delta-pr std dev (Range sensor position).*/
    Eigen::Vector3f dthetar0_std;  /*!< Initial delta-thetar std dev (Range sensor orientation).*/
    char frame; /*!< Frame inwhich the estimation is done. 'g': Global. 'l': Local. */
} ;

/*! Time stamps class */ 
class stamps{
  public:
    stamps()
    {
       // Initialize stamps
       this->t_old = 0.0; 
       this->t_now = 0.0; 
       this->proc_time = 0.0; 
       this->proc_stop = 0.0; 
       this->dt = 0.0;  
    };
    ~stamps(){};    
    float t_old;     /*!< Time used to compute dt (s).*/
    float t_now;     /*!< Time used to compute dt (s).*/
    float proc_time; /*!< Processing Time (s).*/
    float proc_stop; /*!< Time when Robot stopped (s).*/
    float dt;        /*!< Time differential (s).*/
};   

/*! Main ESKF class */ 
class CEskf
{
  private:

    // Filter    
    Eigen::VectorXf x_;                         /*!< Nominal State (1x33: [p v q ab wb g pof qo pr qr]).*/
    Eigen::VectorXf x_atstop_;                  /*!< Nominal State when robot stops.*/
    Eigen::VectorXf dx_;                        /*!< Error State (1x30 : [dp dv dtheta dab dwb dg dpo dthetao dpr dthetar].*/
    Eigen::Vector3f w_;                         /*!< Last Angular velocity reading.*/
    Eigen::MatrixXf P_;                         /*!< Estimation Convariance.*/
    params params_;                             /*!< Filter parameters.*/
    bool first_prop_done_;                      /*!< First propagation flag.*/
    std::mutex mutex_;                          /*!< Mutex to lock/unlock the object.*/
    stamps stamp_;                              /*!< Time stamps.*/

    // Sensors
    std::shared_ptr<Sensor::CImu> imu_;                 /*!< IMU sensor.*/
    std::shared_ptr<Sensor::CPose> pose_;               /*!< Pose sensor (3position+3orientation rates).*/
    std::shared_ptr<Sensor::CPose> pose2_;              /*!< Pose sensor (3position+3orientation rates).*/
    std::shared_ptr<Sensor::CPosition> position_;       /*!< Position sensor (3position).*/
    std::shared_ptr<Sensor::COrientation> orientation_; /*!< Orientation sensor (3orientation rates).*/
    std::shared_ptr<Sensor::CLinvel> linvel_;           /*!< Linear Velocity sensor (3velocities).*/
    std::shared_ptr<Sensor::CRange> range_;             /*!< Range sensor.*/
    std::shared_ptr<Sensor::CPx4_flow> px4_;            /*!< PX4 Optical Flow sensor.*/
    std::shared_ptr<Sensor::CFlow2d> flow2d_;           /*!< 2D Flow sensor.*/

    // Robot
    Robot::CQuadrotor  quadrotor_; /*!< Platform states.*/

    /**
    * \brief Set Time stamps
    *
    * Sets time stamps and computes dt w.r.t last time stamps received.
    */
    void set_time(const float& tstamp);

    /**
    * \brief Set Observation phase
    *
    * Sets the observation values depending on the robot phase.
    * This step is required due to px4 limited range (0.3m<pz<5m)  
    * Different values for the sensor:
    *   -LANDED: sinthetic values (robot not moving).
    *   -TOFF,LANDING: High covariance in order to reduce update stage
    *   -FLYING: sensor readings and corresponding covariances.
    *
    * Inputs:
    *   -flying: Indicates quadrotor is flying or landed
    *   -gnd_dist: Ground distance (to know it is above 30cm -> px4 limits) 
    */
    void set_obs_phase(const bool& flying, const float& gnd_dist);

    /**
    * \brief Set params
    *
    * Set filter parameters locking/unlocking mutex. 
    * It also sets the corresponding estimation matrix (P)
    */
    void set_params(const params& p);

    /**
    * \brief Set the state covariance matrix
    *
    * Sets the P covariance matrix according to the filter parameters.
    */  
    void set_Pcov_mat(void);

    /**
    * \brief Set nominal state
    *
    * Set the nominal state vector locking/unlocking mutex..
    */
    void set_nom_state(const Eigen::VectorXf& x);

    /**
    * \brief Set error state
    *
    * Set the error state vector locking/unlocking mutex..
    */
    void set_e_state(const Eigen::VectorXf& dx);

    /**
    * \brief  Error State Kalman Filter Update 
    *
    * Error State Kalman Filter Update.
    * Fills the nominal state (x) with the previous nominal state and the error state
    * Nominal State vector update considering additive model.
    * It depends on the frame.
    * 
    * Inputs:
    *   There are no specific inputs due to all are class variables.
    */
    void eskf_update(void);

    /**
    * \brief  Error State Kalman Filter Reset 
    *
    * Error State Kalman Filter Reset.
    * Resets the error state (dx) to start the next iteration and 
    * update the estimation covariance matrix (P) accordingly.
    * Depends on the frame.
    *
    * Inputs:
    *   There are no specific inputs due to all are class variables.
    */
    void eskf_reset(void);

    /**
    * \brief  Which is the next sensor?.
    *
    * Returns the next sensor to be processed according to their reading time stamps.
    *
    */  
    bool which_next(Sensor_Name& name, float& tstamp);

    /**
    * \brief Get Sensor readings
    *
    * Generic function that returns the first sensor reading in the buffer. 
    */
    template <typename T_SENSOR>
    Eigen::VectorXf get_sensor_data(T_SENSOR& tS)
    {
      Eigen::VectorXf d;
      this->mutex_.lock();
      d = tS->get_earliest_data().val;
      this->mutex_.unlock();
      return d;
    }

    /**
    * \brief Get Sensor parameters
    *
    * Generic function that returns the sensor parameters.
    *
    */
    template <typename T_SPARAMS, typename T_SENSOR>
    T_SPARAMS get_sensor_params(T_SENSOR& tS)
    {
      return tS->get_params(); 
    }

    /**
    * \brief Set sensor parameters
    *
    * Generic function to set sensor parameters
    */
    template <typename T_SENSOR, typename T_SPARAMS>
    void set_sensor_params(T_SENSOR& tS, const T_SPARAMS& tP)
    {
      this->mutex_.lock();
      tS->set_params(tP);
      this->mutex_.unlock();
    }

    /**
    * \brief Set Sensor Reading
    *
    * Generic function to set new sensor reading
    *
    */
    template <typename T_SENSOR>
    void set_sensor_reading(T_SENSOR& tS,const float& t, const Eigen::VectorXf& val)
    {
      this->mutex_.lock();
      tS->push_data(t,val);
      this->mutex_.unlock();
    }

    /**
    * \brief Correct with Sensor Reading
    *
    * Generic function to correct with a sensor reading
    *
    */
    template <typename T_SENSOR>
    bool sensor_correction(const std::shared_ptr<T_SENSOR>& tSptr)
    {
      bool correction_done = false;  
      if (!this->first_prop_done_ && !tSptr->is_queue_empty())
        tSptr->empty_queue(); // remove previous readings 
      else if(!tSptr->is_queue_empty())
      {
        this->stamp_.proc_time = tSptr->get_earliest_data().tstamp;
        correction_done = tSptr->innovation_and_correction(this->x_, this->dx_, this->P_, this->params_.frame, this->w_);
      }
      return correction_done;
    }

  public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Eigen::Vector3f g_; /*!< Gravity vector.*/

  	/**
    * \brief Constructor
    *
    * Error-State Kalman Filter Class constructor.
    *
    */
    CEskf(void);

  	/**
    * \brief Destructor
    *
    * Error-State Kalman Filter Class destructor.
    *
    */
    ~CEskf(void);

    /**
    * \brief Get nominal-state vector
    *
    * Get nominal-state vector. Function to prevent external modifications.
    *
    */
    Eigen::VectorXf get_x_state(void);

    /**
    * \brief Get error-state vector
    *
    * Get error-state vector. Function to prevent external modifications.
    *
    */
    Eigen::VectorXf get_dx_state(void);

    /**
    * \brief Get processing time
    *
    * Get processing time from current filter step.
    *
    */    
    float get_proc_time(void);

    /**
    * \brief Set Initial Parameters
    *
    * Set Initial parameters
    *
    * Input:
    *   x_state0: Initial Robot state parameters.
    *   f_params: Filter parameters.
    *   imu_params: IMU paramters.
    *   position_params: Position sensor parameters.
    *   orientation_params: Orientation sensor parameters.
    *   range_params: Range sensor parameters.
    *   of_params: PX4 Optical flow sensor parameters.
    *   flow2d_params: Flow2d sensor parameters.
    */
    void set_init_params(const params& f_params, const Eigen::VectorXf& x0, const Eigen::VectorXf& dx0, const Sensor::imu_params& imu, const Sensor::pose_params& pose, const Sensor::pose_params& pose2, const Sensor::position_params& position, const Sensor::orientation_params& orientation, const Sensor::linvel_params& linvel, const Sensor::range_params& range, const Sensor::px4_params& px4, const Sensor::flow2d_params& flow2d);
  
    /**
    * \brief Update filter results 
    *
    * Run filter next step (either propagation or update and correction)
    *
    * Input/Ouput:
    *   - state:  Robot state (6D).
    *   - ang_vel: Robot Angular Velocity (currently not included in the state).
    */    
    bool update(Eigen::VectorXf& state, Eigen::Vector3f& ang_vel, const bool& flying, const float& gnd_dist);


    // *************************************************************************
    // IMU *********************************************************************
    // *************************************************************************

    /**
    * \brief Get IMU readings
    *
    * Get IMU readings for debugging purposes.
    * Returns a vector containing [a;w]; 
    */
    Eigen::VectorXf get_imu_data(void);

    /**
    * \brief Get imu parameters
    *
    * Get imu parameters
    *
    */
    Sensor::imu_params get_imu_params(void);

    /**
    * \brief Set imu parameters
    *
    * Set imu parameters
    *
    */
    void set_imu_params(Sensor::imu_params& imu);

    /**
    * \brief Set IMU Readings
    *
    * Store new IMU readings
    *
    * Inputs:
    *   - t: Time stamp.
    *   - a: Acc. readings (m/s^2). a = [ax,ay,az].
    *   - w: Gyro. readings (rad/s). w = [wx,wy,wz].
    */
    void set_imu_reading(const float& t, const Eigen::Vector3f& a, const Eigen::Vector3f& w);


    // *************************************************************************
    // POSE ********************************************************************
    // *************************************************************************

    /**
    * \brief Get Pose readings
    *
    * Get Pose readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_pose_data(void);

    /**
    * \brief Get Pose 2 readings
    *
    * Get Pose 2 readings for debugging purposes.
    * Returns the range value;
    */
    Eigen::VectorXf get_pose2_data(void);

    /**
    * \brief Get pose parameters
    *
    * Get pose parameters.
    *
    */
    Sensor::pose_params get_pose_params(void);

    /**
    * \brief Get pose 2 parameters
    *
    * Get pose parameters.
    *
    */
    Sensor::pose_params get_pose2_params(void);

    /**
    * \brief Set pose parameters
    *
    * Set pose parameters.
    * Inputs:
    *   - pose: parameters.  
    */
    void set_pose_params(Sensor::pose_params& pose);

    /**
    * \brief Set pose 2 parameters
    *
    * Set pose 2 parameters.
    * Inputs:
    *   - pose 2: parameters.
    */
    void set_pose2_params(Sensor::pose_params& pose);

    /**
    * \brief Set Pose Reading
    *
    * Store new Pose reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Pose reading = [p_x,p_y,p_z,r_x,r_y,r_z].
    */
    void set_pose_reading(const float& t, const Eigen::VectorXf& val);

    /**
    * \brief Set Pose 2 Reading
    *
    * Store new Pose 2 reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Pose reading = [p_x,p_y,p_z,r_x,r_y,r_z].
    */
    void set_pose2_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // POSITION ****************************************************************
    // *************************************************************************

    /**
    * \brief Get Position readings
    *
    * Get Position readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_position_data(void);

    /**
    * \brief Get position parameters
    *
    * Get position parameters
    *
    */
    Sensor::position_params get_position_params(void);

    /**
    * \brief Set position parameters
    *
    * Set position parameters
    * Inputs:
    *   - position: parameters.  
    */
    void set_position_params(Sensor::position_params& position);

    /**
    * \brief Set Position Reading
    *
    * Store new Position reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Position reading = [p_x,p_y,p_z].
    */
    void set_position_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // ORIENTATION *************************************************************
    // *************************************************************************

    /**
    * \brief Get Orientation readings
    *
    * Get Orientation readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_orientation_data(void);

    /**
    * \brief Get orientation parameters
    *
    * Get orientation parameters
    *
    */
    Sensor::orientation_params get_orientation_params(void);

    /**
    * \brief Set orientation parameters
    *
    * Set orientation parameters
    * Inputs:
    *   - orientation: parameters.  
    */
    void set_orientation_params(Sensor::orientation_params& orientation);

    /**
    * \brief Set Orientation Reading
    *
    * Store new Orientation reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Orientation reading = [r_x,r_y,r_z].
    */
    void set_orientation_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // LINEAR VELOCITY *********************************************************
    // *************************************************************************

    /**
    * \brief Get Linear velocity readings
    *
    * Get Linear velocity readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_linvel_data(void);

    /**
    * \brief Get linear velocity parameters
    *
    * Get linear velocity parameters
    *
    */
    Sensor::linvel_params get_linvel_params(void);

    /**
    * \brief Set linear velocity parameters
    *
    * Set linear velocity parameters
    * Inputs:
    *   - linvel: parameters.  
    */
    void set_linvel_params(Sensor::linvel_params& linvel);

    /**
    * \brief Set Linear velocity Reading
    *
    * Store new Linear velocity reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Linear velocity reading = [v_x,v_y,v_z].
    */
    void set_linvel_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // RANGE *******************************************************************
    // *************************************************************************

    /**
    * \brief Get Range readings
    *
    * Get Range readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_range_data(void);

    /**
    * \brief Get range parameters
    *
    * Get range parameters
    *
    */
    Sensor::range_params get_range_params(void);

    /**
    * \brief Set range parameters
    *
    * Set range parameters
    * Inputs:
    *   - range: parameters.  
    */
    void set_range_params(Sensor::range_params& range);

    /**
    * \brief Set Range Reading
    *
    * Store new Range reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Sensor reading.
    */
    void set_range_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // PX4 *********************************************************************
    // *************************************************************************

    /**
    * \brief Get PX4 readings
    *
    * Get PX4 readings for debugging purposes.
    * Returns a vector containing [pz;vx;vy]; 
    */
    Eigen::VectorXf get_px4_data(void);

    /**
    * \brief Get px4 parameters
    *
    * Get px4 parameters
    *
    */
    Sensor::px4_params get_px4_params(void);

    /**
    * \brief Set px4 parameters
    *
    * Set px4 parameters
    * Inputs:
    *   - px4: parameters.  
    */
    void set_px4_params(Sensor::px4_params& px4);

    /**
    * \brief Set PX4 Optical Flow Readings
    *
    * Store new PX4 Optical Flow readings
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Sensor readings: val = [pz;vx;vy].
    *   - flow: Optical flow raw reading flow = [flow_x,flow_y].
    */
    void set_px4_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // FLOW 2D *****************************************************************
    // *************************************************************************

    /**
    * \brief Get Flow 2D readings
    *
    * Get Flow 2D readings for debugging purposes.
    * Returns the range value; 
    */
    Eigen::VectorXf get_flow2d_data(void);

    /**
    * \brief Get flow2d parameters
    *
    * Get flow2d parameters
    *
    */
    Sensor::flow2d_params get_flow2d_params(void);

    /**
    * \brief Set flow2d parameters
    *
    * Set flow2d parameters
    * Inputs:
    *   - flow2d: parameters.  
    */
    void set_flow2d_params(Sensor::flow2d_params& flow2d);

    /**
    * \brief Set Flow 2D Reading
    *
    * Store new Flow 2D reading
    *
    * Inputs:
    *   - t: Time stamp.
    *   - val: Optical flow raw reading flow = [flow_x,flow_y].
    */
    void set_flow2d_reading(const float& t, const Eigen::VectorXf& val);

    // *************************************************************************
    // TEST AND PRINT FUNCTIONS ************************************************
    // *************************************************************************

    /**
    * \brief Print initial parameters
    *
    * This function is used to print the parameters set of the filter, IMU, PX4 
    * and nominal-state and error-state vectors
    *
    */
    void print_ini_params(void);

    /**
    * \brief Filter data simulation.
    *
    * New Nominal state vector from Simulated data using EULER integration (IMU model).
    * Fills the Nominal state vector from accelerations and angular velocities.
    * The body has orientation given by a quaternion in the state vector, with acceleration A 
    * and angular rate W. 
    * Acceleration is measured in the inertial frame where the gravity field is defined.
    *
    * Inputs:
    *   - a: Acceleration (measured in the inertial frame where the gravity field is defined).
    *   - w: Angular rate (measured in the body frame).
    *   - X_std_sim: STD. deviation of the white noise. Applyed to the X sensor readings.
    *   - dt: Time differential with last readings.
    *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.    
    *
    * Ouputs:
    *   - x_state: Nominal state vector.
    */
    void sim_sequence(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& imu_std_sim, const Eigen::VectorXf& position_std_sim, const Eigen::VectorXf& orientation_std_sim, const Eigen::VectorXf& linvel_std_sim, const Eigen::VectorXf& range_std_sim, const Eigen::VectorXf& px4_std_sim, const Eigen::VectorXf& flow2d_std_sim, const float& tstamp, const char& frame);
};

#endif
