#edit the following line to add the librarie's header files
FIND_PATH(eskf_odometry_INCLUDE_DIR 
	NAMES eskf_odometry.h
	PATHS /usr/include /usr/local/include /usr/local/include/eskf_odometry)

FIND_LIBRARY(eskf_odometry_LIBRARY
    NAMES eskf_odometry
    PATHS /usr/lib /usr/local/lib /usr/local/lib/eskf_odometry) 

IF (eskf_odometry_INCLUDE_DIR AND eskf_odometry_LIBRARY)
   SET(eskf_odometry_FOUND TRUE)
ENDIF (eskf_odometry_INCLUDE_DIR AND eskf_odometry_LIBRARY)

IF (eskf_odometry_FOUND)
   IF (NOT eskf_odometry_FIND_QUIETLY)
      MESSAGE(STATUS "Found eskf_odometry: ${eskf_odometry_LIBRARY}")
   ENDIF (NOT eskf_odometry_FIND_QUIETLY)
ELSE (eskf_odometry_FOUND)
   IF (eskf_odometry_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find eskf_odometry")
   ENDIF (eskf_odometry_FIND_REQUIRED)
ENDIF (eskf_odometry_FOUND)

