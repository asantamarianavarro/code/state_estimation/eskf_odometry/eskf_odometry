#include <sensors/px4_flow.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

	CPx4_flow::CPx4_flow(void) : CSensor_base(3)
	{
		this->min_height_ = 0.31;	// Sensor specifications.
		this->max_height_ = 4.0;	// Sensor specifications.
	  this->last_gnd_dist = 0.0; // Initially considered on the ground.
	  this->in_range_count_ = 0;	// Counters to detect if it is in range or not.
	  this->min_height_inliers_ = 20; // Min good values to consider in range.
	  this->range_state_ = PX4RangeBounds::State::BELOW; // Considering robot on the ground.
	}
	 
	CPx4_flow::~CPx4_flow(void)
	{
	}

  void CPx4_flow::simulate_readings(const Eigen::VectorXf& state, const float& t_now, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& px4_std_sim)
  {
    // Simulate PX4 readings
    obs_model(state,frame,ang_vel,px4_std_sim);
    
    // simulate optical flow
    Eigen::Vector2f flow = Eigen::Vector2f::Zero();
    float fw = 2.2705e+03; //pixels
    float fh = 2.6667e+03; //pixels

    if (state(2) > 0.0)
    {
      flow(0) = -0.01*(this->h_(1)-ang_vel(1)*this->h_(0))/(0.35*state(2)/fh);
      flow(1) = -0.01*(this->h_(2)+ang_vel(0)*this->h_(0))/(0.35*state(2)/fw);    
    }
    Eigen::VectorXf val(5);
    val << this->h_,flow;
    push_data(t_now,val);
  }   

	void CPx4_flow::push_data(const float& t, const Eigen::VectorXf& val)
	{
		this->mutex_.lock();
  	px4_data px4read;
		px4read.tstamp = t;
		px4read.uvflow = val.segment(3,2);
    px4read.val = val.segment(0,3);
		this->data_queue_.push(px4read);
		this->mutex_.unlock();
	}

	void CPx4_flow::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
	{
		Eigen::Matrix3f R;
		Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

    // Relative orientation of the sensor (extrinsics calibration)
    Eigen::Matrix3f Ro;
    Eigen::Quaternionf qo(x_state(22),x_state(23),x_state(24),x_state(25));
    atools::q2R(qo,Ro);

    // Relative orientation of the Range sensor (extrinsics calibration)
    Eigen::Matrix3f Rr;
    Eigen::Quaternionf qr(x_state(29),x_state(30),x_state(31),x_state(32));
    atools::q2R(qr,Rr);

		//Measurements noise.
    Eigen::VectorXf nr = Eigen::VectorXf::Zero(1);
    if (std(0)!=0.0)
      nr(0) = atools::get_rand() * std(0); 
    Eigen::VectorXf nf = Eigen::VectorXf::Zero(2);
    if (std(1)!=0.0)
      nf(0) = atools::get_rand() * std(1);
    if (std(2)!=0.0)
      nf(1) = atools::get_rand() * std(2);

		this->mutex_.lock();

    Eigen::MatrixXf Sz = Eigen::MatrixXf::Zero(1,3);
    Sz(2) = 1.0;

    Eigen::MatrixXf Sxy = Eigen::MatrixXf::Zero(2,3);
    Sxy(0,0) = 1.0;
    Sxy(1,1) = 1.0;

    Eigen::Vector3f pr;  // realtive sensor position wrt IMU
    pr << x_state(24),x_state(25),x_state(26);

    Eigen::VectorXf pos_tmp;
    if (!this->params_.using_laser)
      pos_tmp = Sz*(Rr.transpose()*x_state.segment(0,3) + pr);
    else
      pos_tmp = Sz*(Rr.transpose()*R.transpose()*x_state.segment(0,3) + pr);

    this->h_(0) = pos_tmp(0) + nr(0);
		this->h_.segment(1,2) = Sxy*Ro.transpose()*R.transpose()*x_state.segment(3,3) + nf;

		this->mutex_.unlock();

		calc_e_obs_jac(x_state, frame);

    if (!is_queue_empty())
    {
      // Remove outliers
      remove_outliers(x_state, ang_vel);

      // Check
      check_sensor_bounds(this->data_queue_.front().val(0));
    }
	}

	void CPx4_flow::calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame)
	{
		Eigen::Matrix3f R;
		Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

    // Relative orientation of the Optical Flow sensor (extrinsics calibration)
    Eigen::Matrix3f Ro;
    Eigen::Quaternionf qo(x_state(22),x_state(23),x_state(24),x_state(25));
    atools::q2R(qo,Ro);

    // Relative orientation of the Range sensor (extrinsics calibration)
    Eigen::Matrix3f Rr;
    Eigen::Quaternionf qr(x_state(29),x_state(30),x_state(31),x_state(32));
    atools::q2R(qr,Rr);

    Eigen::Matrix3f tmpR;
    Eigen::Matrix3f tmpRr;
    Eigen::Matrix3f tmpRf;
		Eigen::Matrix3f tmpRfo;

		if(frame=='l') // Local frame
		{
      if (!this->params_.using_laser)
        atools::v2skew(Rr.transpose()*x_state.segment(3,3),tmpRr); // 1rst row: z (due to sensor orientation)
      else
      {
        atools::v2skew(R.transpose()*x_state.segment(3,3),tmpR); // 1rst row: z (due to orientation)
        tmpR = Rr.transpose() * tmpR;
        atools::v2skew(Rr.transpose()*R.transpose()*x_state.segment(3,3),tmpRr); // 1rst row: z (due to orientation)
      }  

      atools::v2skew(R.transpose()*x_state.segment(3,3),tmpRf); // 2nd,3rd rows: velocity (due to orientation)
      atools::v2skew(Ro.transpose()*R.transpose()*x_state.segment(3,3),tmpRfo); // 2nd,3rd rows: Flow part (due to sensor orientation)
		}
		else  // Global frame
		{
			Eigen::Matrix3f tmpsk;
			atools::v2skew(x_state.segment(3,3),tmpsk);
      
      if (!this->params_.using_laser)
        tmpRr = Rr.transpose() * tmpsk; //  1rst row: z (due to sensor orientation)
      else
        tmpR = Rr.transpose() * R.transpose() * tmpsk; //  1rst row: z (due to orientation)
        atools::v2skew(R.transpose()*x_state.segment(3,3),tmpsk);
        tmpRr = Rr.transpose() * tmpsk; //  1rst row: z (due to sensor orientation)

      tmpRf = R.transpose() * tmpsk; // 2nd,3rd rows: velocity part (due to orientation)
      atools::v2skew(R.transpose()*x_state.segment(3,3),tmpsk);
  		tmpRfo = Ro.transpose() * tmpsk; // 2nd,3rd rows: flow part (due to sensor orientation)
		} 

		this->mutex_.lock();

    Eigen::MatrixXf Sz = Eigen::MatrixXf::Zero(1,3);
    Sz(2) = 1.0;

    Eigen::MatrixXf Sxy = Eigen::MatrixXf::Zero(2,3);
    Sxy(0,0) = 1.0;
    Sxy(1,1) = 1.0;

    // range part
    if (!this->params_.using_laser)
      this->H_dx_.block(0,0,1,3) = Sz * Rr.transpose(); // position
    else
    {
      this->H_dx_.block(0,0,1,3) = Sz * Rr.transpose() * R.transpose(); // position
      this->H_dx_.block(0,6,1,3) = Sz * tmpR; // orientation
    }
    this->H_dx_.block(0,24,1,3) = Sz; // sensor position
    this->H_dx_.block(0,27,1,3) = Sz * tmpRr; // sensor orientation

    // 2D velocity part
    this->H_dx_.block(1,3,2,3) = Sxy*Ro.transpose()*R.transpose(); // velocity
    this->H_dx_.block(1,6,2,3) = Sxy*Ro.transpose()*tmpRf; // orientation
		this->H_dx_.block(1,27,2,3) = Sxy*tmpRfo; // sensor orientation

		this->mutex_.unlock();
	}

  void CPx4_flow::remove_outliers(const Eigen::VectorXf& x_state, const Eigen::VectorXf& ang_vel)
  {
  	this->mutex_.lock();
		// Detect height outliers
	  if (abs(this->data_queue_.front().val(0)-this->last_gnd_dist)>0.35)
	  {
	  	atools::print("Warning: PX4 ground distance outlier detected: ",yellow);
	  	atools::print("Read value: ",blue);
	  	atools::print(this->data_queue_.front().val(0),white);
	  	this->data_queue_.front().val(0) = NAN;

      // PX4 Camera Data\
      //   FOV = 21 deg. 
      //   CCD_width=4.51 %(mm)
      //   CCD_height=2.88 %(mm)
      //   f=16 %(mm)
      //   img_width=640 %(pix)
      //   img_height=480 %(pix)
      //   fw=img_width*f/CCD_width %pix
      //   fh=img_height*f/CCD_height %pix
      //   f_length = 0.016;
      float fw = 2.2705e+03; //pixels
      float fh = 2.6667e+03; //pixels
  
    	if (std::isnan(this->data_queue_.front().val(0)))
    	{
    		this->data_queue_.front().val(0) = x_state(2);
    		this->data_queue_.front().val(1) = 0.35*(-this->data_queue_.front().uvflow(0)/0.01)*x_state(2)/fh + ang_vel(1)*x_state(2);
    		this->data_queue_.front().val(2) = 0.35*(-this->data_queue_.front().uvflow(1)/0.01)*x_state(2)/fw - ang_vel(0)*x_state(2);  		
    		atools::print(" Corrected with values: ",blue);
  	  	atools::print(this->data_queue_.front().val(0),white);
        atools::print("\n",blue);
    	}
		}	

		// Store last used height
		this->last_gnd_dist = this->data_queue_.front().val(0);

  	this->mutex_.unlock();
  }	

  void CPx4_flow::check_sensor_bounds(const float& gnd_dist)
  {
    if ((gnd_dist > this->min_height_) && (gnd_dist < this->max_height_))
    {
      if(this->in_range_count_ < this->min_height_inliers_)
        this->in_range_count_ = this->in_range_count_ + 1;
    }
    if((gnd_dist < this->min_height_) || (gnd_dist > this->max_height_))
    {
      if(this->in_range_count_ > 0)
        this->in_range_count_ = this->in_range_count_ - 1;
    }

    switch (this->range_state_) 
    {
      case PX4RangeBounds::State::BELOW:
        if (this->in_range_count_ == this->min_height_inliers_)
        {
          this->range_state_ = PX4RangeBounds::State::INSIDE;
          this->params_.std = this->params_.std_insidebounds;
        }
      break;
      case PX4RangeBounds::State::INSIDE:
        if (gnd_dist>this->max_height_)
        {
          this->params_.std = this->params_.std_outsidebounds;
          this->range_state_ = PX4RangeBounds::State::ABOVE;
        }
        if (gnd_dist<this->min_height_)
        {
          this->params_.std = this->params_.std_outsidebounds;
          this->range_state_ = PX4RangeBounds::State::BELOW;
        }
      break;
      case PX4RangeBounds::State::ABOVE:
        if (this->in_range_count_ == this->min_height_inliers_)
        {
          this->params_.std = this->params_.std_insidebounds;
          this->range_state_ = PX4RangeBounds::State::INSIDE;
        }
      break;
    }
  }

} //End of Sensor namespace