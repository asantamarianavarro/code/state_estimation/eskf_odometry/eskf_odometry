# Error-State Kalman Filter based Odometry


This library runs an Error-State Kalman filter to compute an odometry.

The filter propagation is done using an IMU linearized model and corrections using a variety of sensors.

## Dependencies

### atools

You have to install asantamaria tools library:

```
  $ git clone https://github.com/angelsantamaria/atools.git
  $ cd atools/build
  $ cmake -DCMAKE_BUILD_TYPE=Release ..
  $ make
  $ sudo make install
```

### eigen3

**Install** eigen3 in your sistem **following the instructions of the ReadMe file** provided in the tuxfamily package.
You can download it at http://eigen.tuxfamily.org

## Installation

```
  $ git clone https://gitlab.com/asantamarianavarro/code/state_estimation/eskf_odometry/eskf_odometry.git
  $ cd eskf_odometry/build
  $ cmake -DCMAKE_BUILD_TYPE=Release ..
  $ make
  $ sudo make install 
```

## API and documentation

You can create the library documentation locally with 

```
  $ cd build
  $ make doc
```
All documents are created in the /doc/html folder. 

To check the API open **doc/html/index.html** with your favourite web browser. 

## License

Copyright (C) 

Author Angel Santamaria-Navarro (asantamaria@iri.upc.edu)

All rights reserved.

This file is part of eskf_odometry library.

eskf_odometry library is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>


