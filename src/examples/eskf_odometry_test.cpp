#include <eskf_odometry.h>

// atools stuff
#include <debug_fc.h>

CEskf filter_;

float dt = 0.1;   //Time differential
float tstamp = 0; //Time differential
float first_imu_reading = true; // To integrate after second

void gen_quad_data(Eigen::MatrixXf& a, Eigen::MatrixXf& w)
{
  int n_stp = 10; //Number of steps

  // ***************************************************************************
	// Forward movement **********************************************************
	// ***************************************************************************
 	w = Eigen::MatrixXf::Zero(3,n_stp); //Angular Velocity

	float val = 0.1; // Big value because it is reached only during 1 dt (to see results).

	float tsim = 0.0;
	for (int ii = 0; ii < n_stp; ++ii)
	{
		float amp = 1.0;
    tsim = ii * dt;
    w(1,ii) = amp*sin(2*3.14159*tsim);
	}

 	//Accelerations
	Eigen::Vector3f theta = Eigen::Vector3f::Zero();
	a = Eigen::MatrixXf::Zero(3,n_stp);

	// Other acc.
  Eigen::Matrix3f M;
	M.row(0) << 0,1,0;
	M.row(1) << -1,0,0;
	M.row(2) << 0,0,0;
	for (int ii = 0; ii < n_stp; ++ii)
	{
		theta = theta + w.col(ii)*dt;
		Eigen::Vector3f ang;
		ang = theta.array().tan();
		a.col(ii) = M*ang;
	}
}

int main(int argc, char *argv[])
{
	atools::print("\n_____________________________________________________________________\n",cyan);
	atools::print("Demo of a vehicle state estimation using an Error-State Kalman Filter\n",cyan);
	atools::print("                  Simulation with sinthetic data                     \n",cyan);
	atools::print("_____________________________________________________________________\n",cyan);

	atools::print("_______________________________________________________INITIALIZATION\n",yellow);
	atools::print("-- Generating quadrotor data\n",green);

	Eigen::MatrixXf a;
	Eigen::MatrixXf w;
	gen_quad_data(a, w);

	// DEBUG
	atools::print("a (Inertial):\n",blue);
	atools::print(a,white);
	atools::print("\nw:\n",blue);
	atools::print(w,white);
	cout << endl;

	// Set Filter parameters
	params f_params;
	f_params.dp0_std << 0,0,0.1;
	f_params.dv0_std << 0,0,0;
	f_params.dtheta0_std << 0.05,0.05,0.05;
	f_params.dab0_std << 0.03,0.03,0.03;
	f_params.dwb0_std << 0.03,0.03,0.03;
	f_params.dg0_std << 0,0,0;
	f_params.dpo0_std << 0,0,0;
	f_params.dthetao0_std << 0,0,0;
	f_params.dpr0_std << 0,0,0;
	f_params.dthetar0_std << 0,0,0;
	f_params.frame = 'g';

	// Set IMU STD
	Sensor::imu_params imu_params;
	imu_params.std.segment(0,3) << 0.006,0.006,0.006; 
	imu_params.std.segment(3,3) << 0.004,0.004,0.004; 
	imu_params.std.segment(6,3) << 0,0,0; //Calibration
	imu_params.std.segment(9,3) << 0,0,0; //Calibration 
	imu_params.met = 'c';	

	// Set Position STD
	Sensor::position_params position_params;
	position_params.std_outsidebounds << 10.0,10.0,10.0;
	position_params.std_insidebounds << 0.1,0.1,0.1;
	position_params.std << position_params.std_insidebounds; // As a demo, it is considered initially running

	// Set Orientation STD
	Sensor::orientation_params orientation_params;
	orientation_params.std_outsidebounds << 10.0,10.0,10.0;
	orientation_params.std_insidebounds << 0.1,0.1,0.1;
	orientation_params.std << orientation_params.std_insidebounds; // As a demo, it is considered initially running	

	// Set Linear Velocity STD
	Sensor::linvel_params linvel_params;
	linvel_params.std_outsidebounds << 10.0,10.0,10.0;
	linvel_params.std_insidebounds << 0.1,0.1,0.1;
	linvel_params.std << linvel_params.std_insidebounds; // As a demo, it is considered initially running

	// Set Pose STD
    Sensor::pose_params pose_params;
    pose_params.std_outsidebounds << 10.0,10.0,10.0;
    pose_params.std_insidebounds << 0.1,0.1,0.1;
    pose_params.std << pose_params.std_insidebounds; // As a demo, it is considered initially running

    Sensor::pose_params pose2_params;
    pose2_params.std_outsidebounds << 10.0,10.0,10.0;
    pose2_params.std_insidebounds << 0.1,0.1,0.1;
    pose2_params.std << pose2_params.std_insidebounds; // As a demo, it is considered initially running

	// Set Range STD
	Sensor::range_params range_params;
	range_params.std_outsidebounds << 10.0;
	range_params.std_insidebounds << 0.01;
	range_params.std = range_params.std_insidebounds; // As a demo, it is considered initially running

	// Set px4 STD
	Sensor::px4_params px4_params;
	px4_params.std_outsidebounds << 10.0,10.0,10.0;
	px4_params.std_insidebounds << 0.01,0.1,0.1;
	px4_params.std << px4_params.std_insidebounds; // As a demo, it is considered initially running

	// Set Flow2d STD
	Sensor::flow2d_params flow2d_params;
	flow2d_params.std_outsidebounds << 10.0,10.0;
	flow2d_params.std_insidebounds << 0.1,0.1;
	flow2d_params.std << flow2d_params.std_insidebounds; // As a demo, it is considered initially running
	flow2d_params.focal_length = 100;

	// Initialize nominal state vector
	Eigen::VectorXf x_state = Eigen::VectorXf::Zero(33);
	x_state(2) = 2.0;
	x_state(6) = 1.0;
	x_state(22) = 1.0;
	x_state(29) = 1.0;
	x_state.segment(16,3) = filter_.g_;	

	// Initialize error state vector
	Eigen::VectorXf dx_state = Eigen::VectorXf::Zero(30);

	// Set filter parameters
	filter_.set_init_params(f_params,x_state,dx_state,imu_params,pose_params,pose2_params,position_params,orientation_params,linvel_params,range_params,px4_params,flow2d_params);

	// DEBUG: Print already set parameters to verify
	// filter_.print_ini_params();

	// Noise of Simulated sensors 
	Eigen::VectorXf imu_std_sim = Eigen::VectorXf::Zero(12);
	imu_std_sim << 0.006,0.006,0.006,0.004,0.004,0.004,0.0,0.0,0.0,0.0,0.0,0.0;

	Eigen::VectorXf position_std_sim = Eigen::VectorXf::Zero(3);
	position_std_sim << 0.1,0.1,0.1;	

	Eigen::VectorXf orientation_std_sim = Eigen::VectorXf::Zero(3);
	orientation_std_sim << 0.1,0.1,0.1;	

	Eigen::VectorXf linvel_std_sim = Eigen::VectorXf::Zero(3);
	linvel_std_sim << 0.1,0.1,0.1;	

	Eigen::VectorXf range_std_sim = Eigen::VectorXf::Zero(1);
	range_std_sim << 0.01;
	
	Eigen::VectorXf px4_std_sim = Eigen::VectorXf::Zero(3);
	px4_std_sim << 0.01,0.1,0.1;

	Eigen::VectorXf flow2d_std_sim = Eigen::VectorXf::Zero(2);
	flow2d_std_sim << 0.1,0.1;

	atools::print("\n______________________________________________________________RUNNING\n",yellow);

	// Force first IMU push to allow integration in the first loop step.
	filter_.sim_sequence(a.col(0),w.col(0),filter_.g_,imu_std_sim,position_std_sim,orientation_std_sim,linvel_std_sim,range_std_sim,px4_std_sim,flow2d_std_sim,tstamp,f_params.frame);

	for (int ii = 1; ii < a.cols(); ++ii)
	{
		tstamp = tstamp + dt;

		// Simulate sequence and sensor readings
		filter_.sim_sequence(a.col(ii),w.col(ii),filter_.g_,imu_std_sim,position_std_sim,orientation_std_sim,linvel_std_sim,range_std_sim,px4_std_sim,flow2d_std_sim,tstamp,f_params.frame);
		atools::print("\n-- Time: ",cyan);
		atools::print(tstamp,green);
		cout << endl;

		Eigen::VectorXf imu = filter_.get_imu_data();
		atools::print("-- IMU readings (simulation).",magenta);
		atools::print("\na_s: ",blue);
		atools::print(imu.segment(0,3).transpose(),white);
		atools::print("\nw_s: ",blue);
		atools::print(imu.segment(3,3).transpose(),white);
		cout << endl;

		Eigen::VectorXf position = filter_.get_position_data();
		atools::print("-- Position readings (simulation).",magenta);
		atools::print("\n[p_x,p_y,p_z]: ",blue);
		atools::print(position.transpose(),white);		
		cout << endl;

		Eigen::VectorXf orientation = filter_.get_orientation_data();
		atools::print("-- Orientation readings (simulation).",magenta);
		atools::print("\n[r_x,r_y,r_z]: ",blue);
		atools::print(orientation.transpose(),white);		
		cout << endl;

        Eigen::VectorXf pose = filter_.get_pose_data();
        atools::print("-- Pose readings (simulation).",magenta);
        atools::print("\n[p_x,p_y,p_z,roll,pitch,yaw]: ",blue);
        atools::print(pose.transpose(),white);
        cout << endl;

        Eigen::VectorXf pose2 = filter_.get_pose_data();
        atools::print("-- Pose 2 readings (simulation).",magenta);
        atools::print("\n[p_x,p_y,p_z,roll,pitch,yaw]: ",blue);
        atools::print(pose2.transpose(),white);
        cout << endl;

		Eigen::VectorXf linvel = filter_.get_linvel_data();
		atools::print("-- Linear Velocity readings (simulation).",magenta);
		atools::print("\n[v_x,v_y,v_z]: ",blue);
		atools::print(linvel.transpose(),white);		
		cout << endl;

		Eigen::VectorXf range = filter_.get_range_data();
		atools::print("-- Range readings (simulation).",magenta);
		atools::print("\n[pz]: ",blue);
		atools::print(range,white);		
		cout << endl;

		Eigen::VectorXf px4 = filter_.get_px4_data();
		atools::print("-- PX4 readings (simulation).",magenta);
		atools::print("\n[pz;vx;vy]: ",blue);
		atools::print(px4.transpose(),white);		
		cout << endl;

		Eigen::VectorXf flow2d = filter_.get_flow2d_data();
		atools::print("-- Flow2d readings (simulation).",magenta);
		atools::print("\n[flow_x,flow_y]: ",blue);
		atools::print(flow2d.transpose(),white);		
		cout << endl;

		// In this example the propagation, correction and update is serialized, 
		// Data readings and filter run functions can be in parallel threads.
		Eigen::VectorXf state(19,1);
		Eigen::Vector3f ang_vel;
		filter_.update(state,ang_vel,true,px4(0)); // IMU Propagation
		filter_.update(state,ang_vel,true,px4(0)); // POSITION Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // ORIENTATION Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // POSE Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // POSE 2 Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // LINEAR VELOCITY Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // RANGE Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // PX4 Error observation and correction
		filter_.update(state,ang_vel,true,px4(0)); // FLOW2D Error observation and correction

	  atools::print("-- Filter state estimation:",magenta);
 		atools::print("\np: ",blue);
 		atools::print(state.segment(0,3).transpose(),white);
 		atools::print("\nv: ",blue);
 		atools::print(state.segment(3,3).transpose(),white);
 		atools::print("\nq: ",blue);
 		atools::print(state.segment(6,4).transpose(),white);
 		atools::print("\nab: ",blue);
 		atools::print(state.segment(10,3).transpose(),white);
 		atools::print("\nwb: ",blue);
 		atools::print(state.segment(13,3).transpose(),white);
 		atools::print("\ng: ",blue);
 		atools::print(state.segment(16,3).transpose(),white);
 		atools::print("\npo: ",blue);
 		atools::print(state.segment(19,3).transpose(),white);
 		atools::print("\nqo: ",blue);
 		atools::print(state.segment(22,4).transpose(),white);
 		atools::print("\npo: ",blue);
 		atools::print(state.segment(26,3).transpose(),white);
 		atools::print("\nqo: ",blue);
 		atools::print(state.segment(29,4).transpose(),white);
 		cout << endl;
	}
	atools::print("\n________________________________________________________DEMO_FINISHED\n\n",yellow);
}



