#include <sensors/flow2d.h>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/shared_ptr.hpp>

namespace Sensor{

	CFlow2d::CFlow2d(void) : CSensor_base(2)
	{
    // Initialized as PX4 optical flow sensor focal length.

    // PX4 Camera Data\
    //   FOV = 21 deg. 
    //   CCD_width=4.51 %(mm)
    //   CCD_height=2.88 %(mm)
    //   f=16 %(mm)
    //   img_width=640 %(pix)
    //   img_height=480 %(pix)
    //   fw=img_width*f/CCD_width %pix
    //   fh=img_height*f/CCD_height %pix
    //   f_length = 0.016;
    // TODO: It works with this parameters for PX4.
    this->params_.focal_length = 21*0.016*1000/4.51;
	}
	 
	CFlow2d::~CFlow2d(void)
	{
	}

  void CFlow2d::obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
	{
		Eigen::Matrix3f R;
		Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

    // Relative orientation of the sensor (extrinsics calibration)
    Eigen::Matrix3f Ro;
    Eigen::Quaternionf qo(x_state(22),x_state(23),x_state(24),x_state(25));
    atools::q2R(qo,Ro);

		//Measurements noise.
		Eigen::Vector2f n = Eigen::Vector2f::Zero();
    if (std(0)!=0.0)
  		n(0) = atools::get_rand() * std(0); 
    if (std(1)!=0.0)
		  n(1) = atools::get_rand() * std(1);

		this->mutex_.lock();

    float ff = this->params_.focal_length;

		Eigen::Vector2f wtmp;
    wtmp << -ang_vel(1),ang_vel(0);

    Eigen::MatrixXf Sxy = Eigen::MatrixXf::Zero(2,3);
    Sxy(0,0) = 1.0;
    Sxy(1,1) = 1.0;

    this->h_ = Eigen::VectorXf::Zero(2);
    if (x_state(2)>0.0)
      this->h_ = -(ff*Sxy*Ro.transpose()*R.transpose()*x_state.segment(3,3))/x_state(2)  + ff*wtmp + n;

		this->mutex_.unlock();

		calc_e_obs_jac(x_state, frame);
	}

	void CFlow2d::calc_e_obs_jac(const Eigen::VectorXf& x_state, const char& frame)
	{
		Eigen::Matrix3f R;
		Eigen::Quaternionf q(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

    // Relative orientation of the sensor (extrinsics calibration)
    Eigen::Matrix3f Ro;
    Eigen::Quaternionf qo(x_state(22),x_state(23),x_state(24),x_state(25));
    atools::q2R(qo,Ro);

    Eigen::MatrixXf Sxy = Eigen::MatrixXf::Zero(2,3);
    Sxy(0,0) = 1.0;
    Sxy(1,1) = 1.0;

    this->mutex_.lock();
    float ff = this->params_.focal_length;
    this->mutex_.unlock();

    Eigen::Matrix3f tmpR;
    Eigen::Matrix3f tmpRo;
		if(frame=='l') // Local frame
		{
      atools::v2skew(R.transpose()*x_state.segment(3,3),tmpR);
			atools::v2skew(Ro.transpose()*R.transpose()*x_state.segment(3,3),tmpRo);
		}
		else  // Global frame
		{
			Eigen::Matrix3f tmpsk;
			atools::v2skew(x_state.segment(3,3),tmpsk);
			tmpR = R.transpose() * tmpsk;

      atools::v2skew(R.transpose()*x_state.segment(3,3),tmpsk);
      tmpRo = Ro.transpose() * tmpsk;
		}

		this->mutex_.lock();

		this->H_dx_ = Eigen::MatrixXf::Zero(2,30);
    if (x_state(2)>0.0)
    {
      // To avoid the effect of a giant denominator when the sensor is close to the ground, 
      // we update only with the jacobian elements afecting the velocities.
      // this->H_dx_.block(0,2,2,1) << (ff*Sxy*Ro.transpose()*R.transpose()*x_state.segment(3,3))/(x_state(2)*x_state(2));
      this->H_dx_.block(0,3,2,3) << -(ff*Sxy*Ro.transpose()*R.transpose())/x_state(2);
      this->H_dx_.block(0,6,2,3) << -(ff*Sxy*Ro.transpose()*tmpR)/x_state(2);
  		this->H_dx_.block(0,21,2,3) << -(ff*Sxy*tmpRo)/x_state(2);
    }

		this->mutex_.unlock();
	}

} //End of Sensor namespace