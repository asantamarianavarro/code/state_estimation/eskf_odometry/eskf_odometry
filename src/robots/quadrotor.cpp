#include <robots/quadrotor.h>

// atools stuff
#include <debug_fc.h>

using namespace Robot;
using namespace atools;

namespace Robot{

	CQuadrotor::CQuadrotor()
	{
		this->phase = STOPPED;
	}
	 
	CQuadrotor::~CQuadrotor()
	{
	}

	void CQuadrotor::lock()
	{
		this->mutex_.lock();	
	}

	void CQuadrotor::unlock()
	{
		this->mutex_.unlock();		
	}

	void CQuadrotor::set_phase(const quadrotor_phases& p)
	{
		this->mutex_.lock();
		this->phase = p;
		this->mutex_.unlock();
	}

	void CQuadrotor::get_phase(quadrotor_phases& p)
	{
		this->mutex_.lock();
		p = this->phase;
		this->mutex_.unlock();
	}

	void CQuadrotor::print_phase()
	{
		switch (this->phase)
		{
			case STOPPED:
				print("ROBOT: ",white); print("STOPPED\n",red);
				break;
			case RUNNING:
				print("ROBOT: ",white); print("RUNNING\n",green);
				break;
		}
	}
} //End of Robot namespace