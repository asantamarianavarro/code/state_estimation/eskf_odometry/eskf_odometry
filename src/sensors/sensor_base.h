#ifndef _SENSOR_BASE_H
#define _SENSOR_BASE_H

// Std stuff
#include <mutex>
#include <queue>
#include <iostream>
#include <memory>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// atools stuff
#include <alg_fc.h>
#include <debug_fc.h>
#include <rot_fc.h>

// Boost stuff
#include <boost/math/distributions/chi_squared.hpp>

namespace Sensor
{
  /*! Sensor parameters class*/ 
  class CSensor_params
  {
    private:
      int nFields_;
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      Eigen::VectorXf std;               /*!< Std to be used.*/
      Eigen::VectorXf std_outsidebounds; /*!< Measurements std outside bounds.*/
      Eigen::VectorXf std_insidebounds;  /*!< Measurements std working inside bounds.*/       

      /**
      * \brief Constructor
      *
      * Class constructor.
      *
      */       
      CSensor_params(const int& numFields): nFields_(numFields)
      {
        // Std deviations depending on sensor bounds.
        this->std = Eigen::VectorXf::Zero(this->nFields_); 
        this->std_outsidebounds = Eigen::VectorXf::Zero(this->nFields_);  
        this->std_insidebounds = Eigen::VectorXf::Zero(this->nFields_);  
      };

      /**
      * \brief Destructor
      *
      * Class destructor.
      *
      */      
      ~CSensor_params(void){};
  };

  /*! Sensor reading data class*/ 
  class CSensor_data
  {
    private:
      int nFields_;    
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      Eigen::VectorXf val; /*!<  Flow raw data: Flow_u and Flow_v (pixels/frame).*/  
      float tstamp;        /*!<  Time stamp in nano-seconds.*/

      /**
      * \brief Constructor
      *
      * Class constructor.
      *
      */       
      CSensor_data(const int& numFields): nFields_(numFields) 
      {
        this->val = Eigen::VectorXf::Zero(this->nFields_);
        this->tstamp = 0.0;
      };

      /**
      * \brief Destructor
      *
      * Class destructor.
      *
      */      
      ~CSensor_data(void){};
  };  

  /*! Sensor base class*/ 
  // This Super class is created in order to work with pointers to the derived 
  // classes. Otherwise CSensor_base is templated and does not allow it. 
  class CSensor_superbase
  {
  public:
    CSensor_superbase(){};
    virtual ~CSensor_superbase(){};
    
  };

  /*! Sensor base class*/ 
  template <class T_PARAMS, class T_DATA>
  class CSensor_base : public CSensor_superbase
  {
    private:

      int nFields_;          /*!< Number of fields to dynamically allocate objects.*/
      Eigen::VectorXf dz_;   /*!< Residual (Innovation).*/
      Eigen::MatrixXf Z_;    /*!< Residual Covariance (Innovation).*/
      Eigen::MatrixXf K_;    /*!< Kalman gain.*/
      Eigen::MatrixXf Vn_;   /*!< Measurement Covariance matrix.*/

      /**
      * \brief  Compute Observation Residual and Residual Covariance 
      *
      * Compute Observation Residual and Residual Covariance.
      * 
      * Inputs:
      *   - P: Estimation Convariance
      */      
      void residual(const Eigen::MatrixXf& P)
      {
        // Set the corresponding Sensor covariance
        set_cov_mat();

        this->mutex_.lock();
    
        // Compute residual
        this->dz_ = this->data_queue_.front().val - this->h_;
    
        // Delete processed reading
        this->data_queue_.pop();
    
        //Covariance of the residual
        Eigen::MatrixXf Ztmp = this->H_dx_ * P * this->H_dx_.transpose();
        this->Z_ = Ztmp + this->Vn_;
       
        this->mutex_.unlock();
      }

      /**
      * \brief  Error State Kalman Filter Correction 
      *
      * Error State Kalman Filter Correction.
      * Fills the error state vector (dx) and the estimation covariance (P) matrix 
      * considering the observation Jacobian and the residual.
      *
      * Inputs:
      *   - dx: Error State (dp dv dtheta dab dwb dg)
      *   - P: Estimation Convariance
      */
      void correction(Eigen::VectorXf& dx, Eigen::MatrixXf&  P)
      {
        this->mutex_.lock();
    
        //Kalman gain
        this->K_ = (P * this->H_dx_.transpose()) * this->Z_.inverse();
    
        //Correction
        dx = this->K_ * this->dz_;
    
        //New estimation covariance matrix
        P = P - (this->K_*this->Z_*this->K_.transpose());
        P = (P + P.transpose())/2.0; //force symmetric
    
        this->mutex_.unlock();
      };

    public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      
      T_PARAMS params_;               /*!< parameters.*/
      std::queue<T_DATA> data_queue_; /*!< Queue of readings.*/
      Eigen::VectorXf h_;             /*!< Observation model.*/
      Eigen::MatrixXf H_dx_;          /*!< Observation Jacobian r.t. error-state vector.*/
      std::mutex mutex_;              /*!< Mutex to lock/unlock the object.*/

      /**
      * \brief Constructor
      *
      * Class constructor.
      *
      */      
      CSensor_base(const int& numFields) : nFields_(numFields), CSensor_superbase() 
      {
        this->Vn_ = Eigen::MatrixXf::Zero(this->nFields_,this->nFields_);
        this->dz_ = Eigen::VectorXf::Zero(this->nFields_); 
        this->Z_ = Eigen::MatrixXf::Zero(this->nFields_,this->nFields_);  
        this->K_ = Eigen::MatrixXf::Zero(30,this->nFields_);
        this->h_ = Eigen::VectorXf::Zero(this->nFields_);
        this->H_dx_ = Eigen::MatrixXf::Zero(this->nFields_,30); 
      };

      /**
      * \brief Destructor
      *
      * Class destructor.
      *
      */
      virtual ~CSensor_base(void){};

      /**
      * \brief Lock
      *
      * Lock Object.
      *
      */
      void lock(void)
      {this->mutex_.lock();};

      /**
      * \brief Unlock
      *
      * Unlock Object.
      *
      */
      void unlock(void)
      {this->mutex_.unlock();};   

      /**
      * \brief Set parameters
      *
      * Set Sensor parameters. 
      * It also set the sensor covariance matrix according to the parameters.
      *
      */
      void set_params(const T_PARAMS& p)
      { 
        this->mutex_.lock();
        this->params_ = p;
        this->mutex_.unlock();
  
        set_cov_mat();
      };

     /**
      * \brief Set covariance matrix
      *
      * Sets the Sensor covariance matrix according to the object class parameters.
      *
      */  
      void set_cov_mat(void)
      {
        this->mutex_.lock();
        Eigen::VectorXf var = this->params_.std.array() * this->params_.std.array();
        this->Vn_ = var.asDiagonal();
        this->mutex_.unlock();
      };      

      /**
      * \brief Get parameters
      *
      * Get Sensor parameters
      *
      */
      T_PARAMS get_params(void)
      { 
        T_PARAMS p;
        this->mutex_.lock();
        p = this->params_;
        this->mutex_.unlock();
        return p;
      };     

      /**
      * \brief Push data
      *
      * Push data into the buffer (back)
      */
      void push_data(const float& t, const Eigen::VectorXf& val)
      {
        this->mutex_.lock();
        T_DATA reading;
        reading.tstamp = t;
        reading.val = val;
        this->data_queue_.push(reading);
        this->mutex_.unlock();
      };

      /**
      * \brief Print Sensor parameters
      *
      * Print Sensor STD parameters. 
      */
      void print_params(void)
      {
        atools::print("  ______Sensor STD values",green);
        atools::print("\n  std to be used: ",blue);
        atools::print(this->params_.std.transpose(),white);
        atools::print("\n  std_outsidebounds: ",blue);
        atools::print(this->params_.std_outsidebounds.transpose(),white);
        atools::print("\n  std_insidebounds: ",blue);
        atools::print(this->params_.std_insidebounds.transpose(),white);   
        std::cout << std::endl;
      };

      /**
      * \brief PoP data
      *
      * Delete oldest data in the queue (front)
      */
      bool pop_data(void)
      {
        bool pop_done = false;
        this->mutex_.lock();
        if (!this->data_queue_.empty())
        {
          this->data_queue_.pop();
          pop_done = true;
        }
        this->mutex_.unlock();
        return pop_done;
      };

      /**
      * \brief Get earliest data
      *
      * Return the earliest data in the buffer (front)
      */
      T_DATA get_earliest_data(void)
      {
        T_DATA d;
        this->mutex_.lock();
        d = this->data_queue_.front();
        this->mutex_.unlock();
        return d;
      };

      /**
      * \brief Empty queue readings.
      *
      * Empties queue readings.
      */
      void empty_queue(void)
      {
        this->mutex_.lock();
        while(!data_queue_.empty())
          data_queue_.pop();
        this->mutex_.unlock();
      };

      /**
      * \brief Is empty.
      *
      * Returns true if queue is empty.
      */
      bool is_queue_empty(void)
      {
        bool empty;
        this->mutex_.lock();
        empty = this->data_queue_.empty();
        this->mutex_.unlock();
        return empty;
      };

      /**
      * \brief Simulate Range readings
      *
      * Simulate and set Range readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - t_now: Current time stamp. 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      *   - frame: Where to compute the orientation error (local or global)
      *   - ang_vel: Angular velocity.
      *
      * NOTE: Some inputs might not be used.
      */
      void simulate_readings(const Eigen::VectorXf& x_state, const float& t_now, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std)
      {
        obs_model(x_state, frame, ang_vel, std);
        push_data(t_now,this->h_);
      };  

      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      *   - (OPTIONAL):
      *       - frame: Where to compute the orientation error (local or global)
      *       - ang_vel: Angular velocity.
      *
      * TO BE IMPLEMENTED BY THE USER
      *
      * At least it should compute the Observation model (h_), the observation Jacobian (H_dx_).
      */
      // virtual void obs_model(const Eigen::VectorXf& x_state, const Eigen::VectorXf& std){};
      virtual void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std){};

      /**
      * \brief  Error State Kalman Filter Innovation and Correction
      *
      * Error State Kalman Filter Innovation and Correction.
      * Computes the innovation and updates z and its innovation covariance matrix Z 
      * from a measurement N{f,F} and the expectation N{h,H}
      *
      * Inputs:
      *   - x_state: Nominal State (p v q ab wb g)
      *   - dx: Error State (dp dv dtheta dab dwb dg)
      *   - P: Estimation Convariance
      *   - frame: Where to compute the orientation error (local or global)
      *   - ang_vel: Angular velocity.
      *
      * NOTE: Some inputs might not be used.
      */
      bool innovation_and_correction(const Eigen::VectorXf& x_state, Eigen::VectorXf& dx, Eigen::MatrixXf&  P, const char& frame, const Eigen::VectorXf& ang_vel)
      {
        bool correction_done = false;

        obs_model(x_state, frame, ang_vel, Eigen::VectorXf::Zero(nFields_)); // Measurement noise STD (random walk) currently not considered.
        
        residual(P);

        // Discard Outliers
        double dmahalanobis; // Compute mahalanobis distance to remove outliers from correction step
        dmahalanobis = std::sqrt(this->dz_.transpose()*this->Z_.inverse()*this->dz_);
        double alpha = 0.99; // Chi2 quantile
        double Ndof = this->dz_.size();
        boost::math::chi_squared dist(Ndof);
        double ulim = boost::math::quantile(complement(dist, 1-alpha));

        // FIX:
//        if (dmahalanobis < ulim)
//        {
          correction(dx,P);
          correction_done = true;
//        }
        return correction_done;
      }
  };  

} //End of Sensor namespace

#endif
