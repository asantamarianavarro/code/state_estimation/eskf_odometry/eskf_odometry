#ifndef _ORIENTATION_SENSOR_H
#define _ORIENTATION_SENSOR_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{

  /*! Orientation parameters class*/ 
  class orientation_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      orientation_params(void) : CSensor_params(3){};
      ~orientation_params(void){};     
  };

  /*! Orientation data class*/ 
  class orientation_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      orientation_data(void) : CSensor_data(3){};
      ~orientation_data(void){};
  };

  /*! Main Orientation Sensor class*/ 
  class COrientation : public CSensor_base<orientation_params,orientation_data>
  {
    private:

      /**
      * \brief Sensor observation model and Jacobian matrix.
      *
      * Fills the observation matrix and its Jacobian (r.t. error state) 
      * from current nominal state vector.
      * It can be used also to simulate readings
      *
      * Inputs:
      *   - x_state: Nominal state vector.
      *   - frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.  
      *   - ang_vel: Current angular velocity (reading from Gyros). 
      *   - std: STD. deviation of the white noise (random walk). Applyed to the readings. By default is zero.
      */
      void obs_model(const Eigen::VectorXf& x_state, const char& frame, const Eigen::VectorXf& ang_vel, const Eigen::VectorXf& std = Eigen::VectorXf::Zero(3));

      /**
      * \brief Compute Error-state Observation Jacobian
      *
      * Fills the Observation Jacobian matrix r.t. error state vector.
      *
      * Input:
      *   - x_state: Nominal state vector.
      */  
      void calc_e_obs_jac(const Eigen::VectorXf& x_state);

    public:

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

      /**
      * \brief Constructor
      *
      * Optical Flow Class constructor.
      *
      */
      COrientation(void);

      /**
      * \brief Destructor
      *
      * Optical Flow Class destructor.
      *
      */
      ~COrientation(void);
  };

} //End of Sensor namespace

#endif