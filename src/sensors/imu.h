#ifndef _IMU_H
#define _IMU_H

// Std stuff
#include <mutex>
#include <queue>

// Eigen stuff
#include <eigen3/Eigen/Dense>

// Sensor base
#include <sensors/sensor_base.h>

namespace Sensor{

  /*! IMU parameters class*/ 
	class imu_params : public CSensor_params
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      imu_params() : CSensor_params(12)
      {
        this->met = 'd';
      };
      ~imu_params(){};
	    char met;	              /*!< Sets the data time nature. 'c': continous. 'd' discrete.*/
	};

  /*! IMU data class*/ 
	class imu_data : public CSensor_data
  {
    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
      imu_data() : CSensor_data(6)
      {
        this->dt = 0.0;
      };
      ~imu_data(){};
	    float	dt;		        /*!< Time differential since last data. */
	};

  /*! Main IMU Sensor class*/ 
	class CImu : public CSensor_base<imu_params,imu_data>
	{

	  private:

      Eigen::MatrixXf Q;     /*!< Covariance Matrix.*/
      Eigen::VectorXf f;     /*!< Observation matrix.*/
      Eigen::MatrixXf Fx_dx; /*!< Error state IMU transition matrix.*/

    	bool is_first_push; /*!< To detect first message and set last_t according.*/

	  	float time_last; /*!< Time of last reading used to propagate.*/

  		/**
  		* \brief IMU model and Jacobian matrix.
  		*
  		* Fills the 3-acc and 3-gyro measurements corresponding to a moving body in a gravity field.
  		* The body has orientation given by a quaternion in the state vector, with acceleration A 
  		* and angular rate W. 
  		* Acceleration is measured in the inertial frame where the gravity field is defined.
  		*
  		* Inputs:
  		* 	- x_state: Nominal state vector.
  		*   - a: Acceleration (measured in the inertial frame where the gravity field is defined).
  		*   - w: Angular rate (measured in the body frame).
  		*	- std_sim: STD. deviation of the white noise. Applyed to the readings.
  		*	- frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.	 
  		*/
  		void obs_model(const Eigen::VectorXf& x_state, const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::VectorXf& std_sim, const char& frame);

	  	/**
	    * \brief Set covariance matrix.
	    *
	    * Set the covariance matrix according to the object parameters. 
	    * The std parameters of the imu are mapped into the corresponding covariance matrix rows 
	    * inside this function. Thus Q = Fi*Q*Fi', where Fi mapps the corresponding columns.
	    *
	    * Inputs:
	    *	met: Indicates the time metod to be considered. c: Continous time. d: Discrete. 
	    *	   	 This parameter depends on the IMU data source 
      *      (i.e. With a simulated imu should be 'c' and with an IMU hardware sensor 'd')
	    * Outputs:
	    *	Set the object class Q: Covariance matrix of the sensor according to the filter. 
	    * In this case the IMU parameters are mapped with Fi into the correct rows of Q, such as
	    *	Q = Fi*Qi*Fi', where Qi is the original imu covariance matrix. 		
	    */	
		  void set_cov_mat(const float& dt, const char& met='d');

	  	/**
	    * \brief Compute Error-state IMU transition matrix.
	    *
	    * Computes the IMU error-state transition matrix.
	    * The integration method corresponds to the Taylor series expansion 
	    * truncated ath third term. 	
	    *
	    * Input:
	    *	- data:	IMU data, containing acc. and gyro. readings na dt.
	    *	- x_state: Nominal State vector (From last estimation loop). (p v q ab wb g).
	    *	- frame:	Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.	
	    */	
		  void calc_e_trans_mat(const imu_data& data, const Eigen::VectorXf& x_state, const char& frame);

  		/**
  		* \brief IMU data simulation using IMU model and simulated data.
  		*
  		* Fills the Nominal state vector from accelerations and angular velocities.
  		* The body has orientation given by a quaternion in the state vector, with acceleration A 
  		* and angular rate W. 
  		* Acceleration is measured in the inertial frame where the gravity field is defined.
  		*
  		* Inputs:
  		*   - a: Acceleration (measured in the inertial frame where the gravity field is defined).
  		*   - w: Angular rate (measured in the body frame).
  		*	- std_sim: STD. deviation of the white noise. Applyed to the readings.
  		*	- dt: Time differential with last readings.
  		*	- frame: Frame w.r.t. the Transition matrix is computed. 'l': local. 'g':global.	
  		*
  		* Ouputs:
  		* 	- x_state: Nominal state vector.
  		*/
  		void sim_state(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& std_sim, const float& dt, const char& frame, Eigen::VectorXf& x_state);

	  public:
	  	
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	  	/**
	    * \brief Constructor
	    *
	    * Imu Class constructor.
	    *
	    */
	    CImu(void);

	  	/**
	    * \brief Destructor
	    *
	    * Imu Class destructor.
	    *
	    */
	    ~CImu(void);

	  	/**
	    * \brief Print IMU parameters
	    *
	    * Print IMU STD parameters for acc. and gyros.	
	    */
		  void print_params(void);

     	/**
	    * \brief Push data
	    *
	    * Push data into the buffer (back)
	    */
			void push_data(const float& t, const Eigen::Vector3f& a, const Eigen::Vector3f& w);

      /**
      * \brief Is first reading.
      *
      * Returns true if it is the first reading to be processed.
      * Required because we need two reading to integrate for first time.
      */
      bool is_first_reading(void);

	  	/**
	    * \brief Get earliest data
	    *
	    * Return the earliest data in the buffer (front)
	    */
			// imu_data get_earliest_data(void);

	  	/**
	    * \brief Simulate IMU readings
	    *
	    * Simulate and set IMU readings
	    *
	    */
			void simulate_readings(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& imu_std_sim, const float& dt, const char& frame, Eigen::VectorXf& state, const float& t_now);

      /**
      * \brief  State propagation using IMU model and readings.
      *
      * Updates the current nominal state vector from 3-acc and 3-gyro measurements.
      * Corresponds to a prediction using the IMU sensor model. Euler Integration.  
      *
      * Inputs:
      *   - imu:  IMU data with a,w and dt.
      *   - x_state: Current nominal state vector that will be updated with new values [p v q ab wb g].
      *   - frame: Where to compute the orientation error.
      *   - P: Estimation Convariance.
      */      
  		void prop_state(const Sensor::imu_data& imu, Eigen::VectorXf& x_state, const char& frame, Eigen::MatrixXf& P);
	};

} //End of Sensor namespace

#endif