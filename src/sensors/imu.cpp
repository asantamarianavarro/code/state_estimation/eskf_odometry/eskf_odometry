#include <sensors/imu.h>

// atools stuff
#include <rot_fc.h>
#include <debug_fc.h>
#include <alg_fc.h>

namespace Sensor{

	CImu::CImu() : CSensor_base(30)
	{
	  this->Q = Eigen::MatrixXf::Zero(30,30);
	  this->Fx_dx = Eigen::MatrixXf::Zero(30,30);

	  this->is_first_push = true;
	  this->time_last = 0.0;
	}
	 
	CImu::~CImu()
	{}

	void CImu::push_data(const float& t, const Eigen::Vector3f& a, const Eigen::Vector3f& w)
	{
		this->mutex_.lock();

		float t_last;

		if (this->is_first_push)
		{
			this->time_last = t;
			this->is_first_push = false;
		}
		else
		{
			t_last = this->time_last;
			this->time_last = t;

			imu_data reading;
			reading.tstamp = t;
			reading.dt = t-t_last; 
			reading.val << a, w;
			this->data_queue_.push(reading);
		}
		this->mutex_.unlock();
	}

	bool CImu::is_first_reading()
	{
		bool first = false;
		this->mutex_.lock();
		first = this->is_first_push;
		this->mutex_.unlock();
		return first;
	}

	void CImu::set_cov_mat(const float& dt, const char& met)
	{
		// Variance for Continuous time IMU readings
		this->mutex_.lock();
		Eigen::VectorXf var_imu_cont(12,1);
		var_imu_cont.block(0,0,3,1) = this->params_.std.segment(0,3).array() * this->params_.std.segment(0,3).array();	 
		var_imu_cont.block(3,0,3,1) = this->params_.std.segment(3,3).array() * this->params_.std.segment(3,3).array();
		var_imu_cont.block(6,0,3,1) = this->params_.std.segment(6,3).array() * this->params_.std.segment(6,3).array();
		var_imu_cont.block(9,0,3,1) = this->params_.std.segment(9,3).array() * this->params_.std.segment(9,3).array();
		this->mutex_.unlock();		

		Eigen::MatrixXf Qi = Eigen::MatrixXf::Zero(12,12);

		if (met=='d')
		{
  			// Variance for Discrete time IMU readings
			Eigen::MatrixXf var_imu_dis = Eigen::MatrixXf::Zero(12,1);
			var_imu_dis.block(0,0,3,1) = var_imu_cont.block(0,0,3,1) * pow(dt,2); 
			var_imu_dis.block(3,0,3,1) = var_imu_cont.block(3,0,3,1) * pow(dt,2);
			var_imu_dis.block(6,0,3,1) = var_imu_cont.block(6,0,3,1) * dt;
			var_imu_dis.block(9,0,3,1) = var_imu_cont.block(9,0,3,1) * dt;

			// IMU discrete time Covariance matrix
			Qi = var_imu_dis.asDiagonal();
 		}
		else
		{
			Eigen::MatrixXf diag = Eigen::MatrixXf::Zero(12,12);
			diag = var_imu_cont.asDiagonal();
			// IMU continuous time Covariance matrix
			Qi = dt * diag;	
		}

		// Jacobian to map the variances to the correct Nominal State vector variables
		Eigen::MatrixXf Fi = Eigen::MatrixXf::Zero(30,12);
		Fi.block(3,0,12,12) = Eigen::MatrixXf::Identity(12,12);

		this->mutex_.lock();
		this->Q = Fi*Qi*Fi.transpose();
		this->mutex_.unlock();		
	}

	void CImu::sim_state(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& std_sim, const float& dt, const char& frame, Eigen::VectorXf& x_state)
	{
		this->mutex_.lock();

		// Linear position in Inertial frame (integrating vel. and acc.)
		Eigen::Vector3f xp = x_state.segment(0,3) + x_state.segment(3,3)*dt + 0.5*a*pow(dt,2);

		// Linear velocity in Inertial frame (integrating acc.)
		Eigen::Vector3f xv = x_state.segment(3,3) + a*dt;

		//Relative rotation due to last angular velocity
		Eigen::Quaternionf q = Quaternionf(x_state(6),x_state(7),x_state(8),x_state(9));

		Eigen::Quaternionf xq;
		atools::qPredict(q,w,xq,dt,1);
		xq = xq.normalized(); 

		//Biases noise.
		Eigen::Vector3f nab,nwb;
		srand (time(NULL));
		nab(0) = ((float) rand()/(RAND_MAX)) * std_sim(6); 
		nab(1) = ((float) rand()/(RAND_MAX)) * std_sim(7);
		nab(2) = ((float) rand()/(RAND_MAX)) * std_sim(8);
		nwb(0) = ((float) rand()/(RAND_MAX)) * std_sim(9);
		nwb(1) = ((float) rand()/(RAND_MAX)) * std_sim(10);
		nwb(2) = ((float) rand()/(RAND_MAX)) * std_sim(11);

    //Biases noise   
   	x_state.segment(10,3) = x_state.segment(10,3) + nab;
   	x_state.segment(13,3) = x_state.segment(13,3) + nwb;

		//Update the state_vector
		x_state.segment(0,3) = xp; 
		x_state.segment(3,3) = xv;
		x_state.segment(6,4) << xq.w(),xq.vec();
		x_state.segment(16,3) = g;

    //OF Sensor position
   	x_state.segment(19,3) = x_state.segment(19,3);
    //OF Sensor orientation
   	x_state.segment(22,4) = x_state.segment(22,4);

    //Range Sensor position
   	x_state.segment(26,3) = x_state.segment(26,3);
    //Range Sensor orientation
   	x_state.segment(29,4) = x_state.segment(29,4);

		this->mutex_.unlock();
	}

	void CImu::simulate_readings(const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::Vector3f& g, const Eigen::VectorXf& imu_std_sim, const float& dt, const char& frame, Eigen::VectorXf& state, const float& t_now)
	{		
		// Simulate platform movements in state
		sim_state(a,w,g,imu_std_sim,dt,frame,state);	

	  // Simulate IMU readings
	  obs_model(state,a,w,imu_std_sim,frame);

	  // Set new synthetic reading
 	  push_data(t_now,this->f.segment(0,3),this->f.segment(3,3));
	}

	void CImu::print_params()
	{
		atools::print("  ______IMU values (STD)",green);
		atools::print("\n  a_std: ",blue);
		atools::print(this->params_.std.segment(0,3).transpose(),white);
		atools::print("\n  w_std: ",blue);
		atools::print(this->params_.std.segment(3,3).transpose(),white);
		atools::print("\n  ab_std: ",blue);
		atools::print(this->params_.std.segment(6,3).transpose(),white);
		atools::print("\n  wb_std: ",blue);
		atools::print(this->params_.std.segment(9,3).transpose(),white);
		cout << endl;
	}

	void CImu::obs_model(const Eigen::VectorXf& x_state, const Eigen::Vector3f& a, const Eigen::Vector3f& w, const Eigen::VectorXf& std_sim, const char& frame)
	{
		Eigen::Matrix3f R;
		Eigen::Quaternionf q = Quaternionf(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

		//Measurements w/o noise
		Eigen::Vector3f am = R.transpose()*(a-x_state.segment(16,3)) + x_state.segment(10,3);
		Eigen::Vector3f wm = w + x_state.segment(13,3);

		//Measurements noise.
		Eigen::Vector3f na,nw;
		na(0) = atools::get_rand() * std_sim(0); 
		na(1) = atools::get_rand() * std_sim(1);
		na(2) = atools::get_rand() * std_sim(2);
		nw(0) = atools::get_rand() * std_sim(3);
		nw(1) = atools::get_rand() * std_sim(4);
		nw(2) = atools::get_rand() * std_sim(5);

		this->mutex_.lock();
		this->f = Eigen::VectorXf::Zero(6);

		this->f.segment(0,3) = am + na;
		this->f.segment(3,3) = wm + nw;
		this->mutex_.unlock();

		if(!is_queue_empty()) // Avoid Jacobian in first IMU reading (integration requirement).
		{
		  calc_e_trans_mat(get_earliest_data(),x_state,frame);
		}
	}

  void CImu::calc_e_trans_mat(const imu_data& data, const Eigen::VectorXf& x_state, const char& frame)
	{
  	Eigen::Matrix3f R;
		Eigen::Quaternionf q = Quaternionf(x_state(6),x_state(7),x_state(8),x_state(9));
		atools::q2R(q,R);

		Eigen::Matrix3f Rw;
		Eigen::Vector3f vec;
		vec << (data.val.segment(3,3)-x_state.segment(13,3))*data.dt;
		atools::v2R(vec,Rw);

		Eigen::Matrix3f Pv = Eigen::Matrix3f::Identity();
		Eigen::Matrix3f Va = -R;
		Eigen::Matrix3f Vg = Eigen::Matrix3f::Identity();

		Eigen::Matrix3f sk_a;
		Eigen::Matrix3f Vtheta;
		Eigen::Matrix3f Thetatheta;
		Eigen::Matrix3f Trunc_sigma0;
		Eigen::Matrix3f Thetaomega;
		Eigen::Matrix3f sk_Ra;

		if(frame=='l') // Local frame
		{
			atools::v2skew((data.val.segment(0,3)-x_state.segment(10,3)),sk_a);
			Vtheta = -R * sk_a;
	  	atools::v2skew((data.val.segment(3,3)-x_state.segment(13,3)),Thetatheta);
			Trunc_sigma0 = Rw.transpose();
			Thetaomega = -Eigen::Matrix3f::Identity();
		}
		else // Global frame
		{
			atools::v2skew(R*(data.val.segment(0,3)-x_state.segment(10,3)),sk_Ra);
			Vtheta = -sk_Ra;
			Thetatheta = Eigen::Matrix3f::Zero();
			Trunc_sigma0 = Eigen::Matrix3f::Identity();
			Thetaomega = -R;
		}	

		Eigen::Matrix3f tmp = (1.0/6.0)*(Thetatheta.array()*Thetatheta.array())*pow(data.dt,3);
		Eigen::Matrix3f Trunc_sigma1 = (Eigen::Matrix3f::Identity()*data.dt) + (0.5*Thetatheta*pow(data.dt,2)) + tmp;
		Eigen::Matrix3f Trunc_sigma2 = (0.5*Eigen::Matrix3f::Identity()*pow(data.dt,2))+((1.0/6.0)*Thetatheta*pow(data.dt,3));
		Eigen::Matrix3f Trunc_sigma3=(1.0/6.0)*Eigen::Matrix3f::Identity()*pow(data.dt,3);

		// IMU transition matrix as a Taylor series expansion, truncated at third term.
		this->mutex_.lock();
		
		this->Fx_dx = Eigen::MatrixXf::Identity(30,30);
		this->Fx_dx.block(0,3,3,3) = Pv*data.dt;
		this->Fx_dx.block(0,6,3,3) = Pv*Vtheta*Trunc_sigma2;
		this->Fx_dx.block(0,9,3,3) = 0.5*Pv*Va*pow(data.dt,2);
		this->Fx_dx.block(0,12,3,3) = Pv*Vtheta*Trunc_sigma3*Thetaomega;
		this->Fx_dx.block(0,15,3,3) = 0.5*Pv*Vg*pow(data.dt,2);
		this->Fx_dx.block(3,6,3,3) = Vtheta*Trunc_sigma1;
		this->Fx_dx.block(3,9,3,3) = Va*data.dt;
		this->Fx_dx.block(3,12,3,3) = Vtheta*Trunc_sigma2*Thetaomega;
		this->Fx_dx.block(3,15,3,3) = Vg*data.dt;
		this->Fx_dx.block(6,6,3,3) = Trunc_sigma0;
		this->Fx_dx.block(6,12,3,3) = Trunc_sigma1*Thetaomega;

		this->mutex_.unlock();
	}

	void CImu::prop_state(const Sensor::imu_data& imu, Eigen::VectorXf& x_state, const char& frame, Eigen::MatrixXf& P)
	{
		// Set IMU covariance matrix
		set_cov_mat(imu.dt);

		//Angular velocity (w/o bias)
		Eigen::Vector3f xw = imu.val.segment(3,3) - x_state.segment(13,3);

		//Relative rotation due to last angular velocity
		Eigen::Quaternionf q = Quaternionf(x_state(6),x_state(7),x_state(8),x_state(9));
		Eigen::Quaternionf xq;

		if (imu.dt>0.0)
			atools::qPredict(q,xw,xq,imu.dt,1);
		xq = xq.normalized(); 

		// Propagate state
		Eigen::Matrix3f R;
		atools::q2R(xq,R);
		Eigen::Vector3f xa = R*(imu.val.segment(0,3)-x_state.segment(10,3))+x_state.segment(16,3);
		Eigen::Vector3f xp = x_state.segment(0,3) + x_state.segment(3,3)*imu.dt + 0.5*xa*pow(imu.dt,2);
		Eigen::Vector3f xv = x_state.segment(3,3) + xa*imu.dt;

		//Update the state_vector
		//Biases and gravity are considered linear during integration. No modifications are done.
		x_state.segment(0,3) = xp; 
		x_state.segment(3,3) = xv;
		x_state.segment(6,4) << xq.w(),xq.vec();

		// Calculate error transition matrix
		calc_e_trans_mat(imu,x_state,frame);

		// Propagate covariance
		this->mutex_.lock();
		P = this->Fx_dx*P*this->Fx_dx.transpose()+this->Q;
		P = (P + P.transpose())/2.0; //force symmetric

		// Delete processed imu reading
		this->data_queue_.pop();

		this->mutex_.unlock();
	}
} //End of Sensor namespace